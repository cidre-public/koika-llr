(*! Definitions related to the fct3 instruction field !*)

Require Import Koika.Frontend.
Require Import rv.Instructions rv.IFields rv.ITypes rv.InstructionsOpcodes.

Inductive fct3_type :=
  | fct3_000 | fct3_001 | fct3_010 | fct3_011 | fct3_100 | fct3_101 | fct3_110
  | fct3_111.
Scheme Equality for fct3_type.

Definition fct3_bin (f : fct3_type) :=
  match f with
  | fct3_000 => Ob~0~0~0
  | fct3_001 => Ob~0~0~1
  | fct3_010 => Ob~0~1~0
  | fct3_011 => Ob~0~1~1
  | fct3_100 => Ob~1~0~0
  | fct3_101 => Ob~1~0~1
  | fct3_110 => Ob~1~1~0
  | fct3_111 => Ob~1~1~1
  end.

Definition instruction_fct3 :
  forall (i : instruction), has_fct3 (get_instruction_i_type i) = true
  -> fct3_type.
Proof.
refine (fun i =>
  match i with
  | JALR_32I           => fun _ => fct3_000
  | BEQ_32I            => fun _ => fct3_000
  | BNE_32I            => fun _ => fct3_001
  | BLT_32I            => fun _ => fct3_100
  | BGE_32I            => fun _ => fct3_101
  | BLTU_32I           => fun _ => fct3_110
  | BGEU_32I           => fun _ => fct3_111
  | LB_32I             => fun _ => fct3_000
  | LH_32I             => fun _ => fct3_001
  | LW_32I             => fun _ => fct3_010
  | LBU_32I            => fun _ => fct3_100
  | LHU_32I            => fun _ => fct3_101
  | SB_32I             => fun _ => fct3_000
  | SH_32I             => fun _ => fct3_001
  | SW_32I             => fun _ => fct3_010
  | ADDI_32I           => fun _ => fct3_000
  | SLTI_32I           => fun _ => fct3_010
  | SLTIU_32I          => fun _ => fct3_011
  | XORI_32I           => fun _ => fct3_100
  | ORI_32I            => fun _ => fct3_110
  | ANDI_32I           => fun _ => fct3_111
  | SLLI_32I           => fun _ => fct3_001
  | SRLI_32I           => fun _ => fct3_101
  | SRAI_32I           => fun _ => fct3_101
  | ADD_32I            => fun _ => fct3_000
  | SUB_32I            => fun _ => fct3_000
  | SLL_32I            => fun _ => fct3_001
  | SLT_32I            => fun _ => fct3_010
  | SLTU_32I           => fun _ => fct3_011
  | XOR_32I            => fun _ => fct3_100
  | SRL_32I            => fun _ => fct3_101
  | SRA_32I            => fun _ => fct3_101
  | OR_32I             => fun _ => fct3_110
  | AND_32I            => fun _ => fct3_111
  | FENCE_32I          => fun _ => fct3_000
  | ECALL_32I          => fun _ => fct3_000
  | EBREAK_32I         => fun _ => fct3_000
  | JALR_64I           => fun _ => fct3_000
  | BEQ_64I            => fun _ => fct3_000
  | BNE_64I            => fun _ => fct3_001
  | BLT_64I            => fun _ => fct3_100
  | BGE_64I            => fun _ => fct3_101
  | BLTU_64I           => fun _ => fct3_110
  | BGEU_64I           => fun _ => fct3_111
  | LB_64I             => fun _ => fct3_000
  | LH_64I             => fun _ => fct3_001
  | LW_64I             => fun _ => fct3_010
  | LBU_64I            => fun _ => fct3_100
  | LHU_64I            => fun _ => fct3_101
  | SB_64I             => fun _ => fct3_000
  | SH_64I             => fun _ => fct3_001
  | SW_64I             => fun _ => fct3_010
  | ADDI_64I           => fun _ => fct3_000
  | SLTI_64I           => fun _ => fct3_010
  | SLTIU_64I          => fun _ => fct3_011
  | XORI_64I           => fun _ => fct3_100
  | ORI_64I            => fun _ => fct3_110
  | ANDI_64I           => fun _ => fct3_111
  | SLLI_64I           => fun _ => fct3_001
  | SRLI_64I           => fun _ => fct3_101
  | SRAI_64I           => fun _ => fct3_101
  | ADD_64I            => fun _ => fct3_000
  | SUB_64I            => fun _ => fct3_000
  | SLL_64I            => fun _ => fct3_001
  | SLT_64I            => fun _ => fct3_010
  | SLTU_64I           => fun _ => fct3_011
  | XOR_64I            => fun _ => fct3_100
  | SRL_64I            => fun _ => fct3_101
  | SRA_64I            => fun _ => fct3_101
  | OR_64I             => fun _ => fct3_110
  | AND_64I            => fun _ => fct3_111
  | FENCE_64I          => fun _ => fct3_000
  | ECALL_64I          => fun _ => fct3_000
  | EBREAK_64I         => fun _ => fct3_000
  | LWU_64I            => fun _ => fct3_110
  | LD_64I             => fun _ => fct3_011
  | SD_64I             => fun _ => fct3_011
  | ADDIW_64I          => fun _ => fct3_000
  | SLLIW_64I          => fun _ => fct3_001
  | SRLIW_64I          => fun _ => fct3_101
  | SRAIW_64I          => fun _ => fct3_101
  | ADDW_64I           => fun _ => fct3_000
  | SUBW_64I           => fun _ => fct3_000
  | SLLW_64I           => fun _ => fct3_001
  | SRLW_64I           => fun _ => fct3_101
  | SRAW_64I           => fun _ => fct3_101
  | FENCE_I_32Zifencei => fun _ => fct3_001
  | FENCE_I_64Zifencei => fun _ => fct3_001
  | CSRRW_32Zicsr      => fun _ => fct3_001
  | CSRRS_32Zicsr      => fun _ => fct3_010
  | CSRRC_32Zicsr      => fun _ => fct3_011
  | CSRRWI_32Zicsr     => fun _ => fct3_101
  | CSRRSI_32Zicsr     => fun _ => fct3_110
  | CSRRCI_32Zicsr     => fun _ => fct3_111
  | CSRRW_64Zicsr      => fun _ => fct3_001
  | CSRRS_64Zicsr      => fun _ => fct3_010
  | CSRRC_64Zicsr      => fun _ => fct3_011
  | CSRRWI_64Zicsr     => fun _ => fct3_101
  | CSRRSI_64Zicsr     => fun _ => fct3_110
  | CSRRCI_64Zicsr     => fun _ => fct3_111
  | MUL_32M            => fun _ => fct3_000
  | MULH_32M           => fun _ => fct3_001
  | MULHSU_32M         => fun _ => fct3_010
  | MULHU_32M          => fun _ => fct3_011
  | DIV_32M            => fun _ => fct3_100
  | DIVU_32M           => fun _ => fct3_101
  | REM_32M            => fun _ => fct3_110
  | REMU_32M           => fun _ => fct3_111
  | MUL_64M            => fun _ => fct3_000
  | MULH_64M           => fun _ => fct3_001
  | MULHSU_64M         => fun _ => fct3_010
  | MULHU_64M          => fun _ => fct3_011
  | DIV_64M            => fun _ => fct3_100
  | DIVU_64M           => fun _ => fct3_101
  | REM_64M            => fun _ => fct3_110
  | REMU_64M           => fun _ => fct3_111
  | MULW_64M           => fun _ => fct3_000
  | DIVW_64M           => fun _ => fct3_100
  | DIVUW_64M          => fun _ => fct3_101
  | REMW_64M           => fun _ => fct3_110
  | REMUW_64M          => fun _ => fct3_111
  | LR_W_00_32A        => fun _ => fct3_010
  | LR_W_01_32A        => fun _ => fct3_010
  | LR_W_10_32A        => fun _ => fct3_010
  | LR_W_11_32A        => fun _ => fct3_010
  | SC_W_00_32A        => fun _ => fct3_010
  | SC_W_01_32A        => fun _ => fct3_010
  | SC_W_10_32A        => fun _ => fct3_010
  | SC_W_11_32A        => fun _ => fct3_010
  | AMOSWAP_W_00_32A   => fun _ => fct3_010
  | AMOSWAP_W_01_32A   => fun _ => fct3_010
  | AMOSWAP_W_10_32A   => fun _ => fct3_010
  | AMOSWAP_W_11_32A   => fun _ => fct3_010
  | AMOADD_W_00_32A    => fun _ => fct3_010
  | AMOADD_W_01_32A    => fun _ => fct3_010
  | AMOADD_W_10_32A    => fun _ => fct3_010
  | AMOADD_W_11_32A    => fun _ => fct3_010
  | AMOXOR_W_00_32A    => fun _ => fct3_010
  | AMOXOR_W_01_32A    => fun _ => fct3_010
  | AMOXOR_W_10_32A    => fun _ => fct3_010
  | AMOXOR_W_11_32A    => fun _ => fct3_010
  | AMOAND_W_00_32A    => fun _ => fct3_010
  | AMOAND_W_01_32A    => fun _ => fct3_010
  | AMOAND_W_10_32A    => fun _ => fct3_010
  | AMOAND_W_11_32A    => fun _ => fct3_010
  | AMOOR_W_00_32A     => fun _ => fct3_010
  | AMOOR_W_01_32A     => fun _ => fct3_010
  | AMOOR_W_10_32A     => fun _ => fct3_010
  | AMOOR_W_11_32A     => fun _ => fct3_010
  | AMOMIN_W_00_32A    => fun _ => fct3_010
  | AMOMIN_W_01_32A    => fun _ => fct3_010
  | AMOMIN_W_10_32A    => fun _ => fct3_010
  | AMOMIN_W_11_32A    => fun _ => fct3_010
  | AMOMAX_W_00_32A    => fun _ => fct3_010
  | AMOMAX_W_01_32A    => fun _ => fct3_010
  | AMOMAX_W_10_32A    => fun _ => fct3_010
  | AMOMAX_W_11_32A    => fun _ => fct3_010
  | AMOMINU_W_00_32A   => fun _ => fct3_010
  | AMOMINU_W_01_32A   => fun _ => fct3_010
  | AMOMINU_W_10_32A   => fun _ => fct3_010
  | AMOMINU_W_11_32A   => fun _ => fct3_010
  | AMOMAXU_W_00_32A   => fun _ => fct3_010
  | AMOMAXU_W_01_32A   => fun _ => fct3_010
  | AMOMAXU_W_10_32A   => fun _ => fct3_010
  | AMOMAXU_W_11_32A   => fun _ => fct3_010
  | LR_W_00_64A        => fun _ => fct3_010
  | LR_W_01_64A        => fun _ => fct3_010
  | LR_W_10_64A        => fun _ => fct3_010
  | LR_W_11_64A        => fun _ => fct3_010
  | SC_W_00_64A        => fun _ => fct3_010
  | SC_W_01_64A        => fun _ => fct3_010
  | SC_W_10_64A        => fun _ => fct3_010
  | SC_W_11_64A        => fun _ => fct3_010
  | AMOSWAP_W_00_64A   => fun _ => fct3_010
  | AMOSWAP_W_01_64A   => fun _ => fct3_010
  | AMOSWAP_W_10_64A   => fun _ => fct3_010
  | AMOSWAP_W_11_64A   => fun _ => fct3_010
  | AMOADD_W_00_64A    => fun _ => fct3_010
  | AMOADD_W_01_64A    => fun _ => fct3_010
  | AMOADD_W_10_64A    => fun _ => fct3_010
  | AMOADD_W_11_64A    => fun _ => fct3_010
  | AMOXOR_W_00_64A    => fun _ => fct3_010
  | AMOXOR_W_01_64A    => fun _ => fct3_010
  | AMOXOR_W_10_64A    => fun _ => fct3_010
  | AMOXOR_W_11_64A    => fun _ => fct3_010
  | AMOAND_W_00_64A    => fun _ => fct3_010
  | AMOAND_W_01_64A    => fun _ => fct3_010
  | AMOAND_W_10_64A    => fun _ => fct3_010
  | AMOAND_W_11_64A    => fun _ => fct3_010
  | AMOOR_W_00_64A     => fun _ => fct3_010
  | AMOOR_W_01_64A     => fun _ => fct3_010
  | AMOOR_W_10_64A     => fun _ => fct3_010
  | AMOOR_W_11_64A     => fun _ => fct3_010
  | AMOMIN_W_00_64A    => fun _ => fct3_010
  | AMOMIN_W_01_64A    => fun _ => fct3_010
  | AMOMIN_W_10_64A    => fun _ => fct3_010
  | AMOMIN_W_11_64A    => fun _ => fct3_010
  | AMOMAX_W_00_64A    => fun _ => fct3_010
  | AMOMAX_W_01_64A    => fun _ => fct3_010
  | AMOMAX_W_10_64A    => fun _ => fct3_010
  | AMOMAX_W_11_64A    => fun _ => fct3_010
  | AMOMINU_W_00_64A   => fun _ => fct3_010
  | AMOMINU_W_01_64A   => fun _ => fct3_010
  | AMOMINU_W_10_64A   => fun _ => fct3_010
  | AMOMINU_W_11_64A   => fun _ => fct3_010
  | AMOMAXU_W_00_64A   => fun _ => fct3_010
  | AMOMAXU_W_01_64A   => fun _ => fct3_010
  | AMOMAXU_W_10_64A   => fun _ => fct3_010
  | AMOMAXU_W_11_64A   => fun _ => fct3_010
  | LR_D_00_64A        => fun _ => fct3_011
  | LR_D_01_64A        => fun _ => fct3_011
  | LR_D_10_64A        => fun _ => fct3_011
  | LR_D_11_64A        => fun _ => fct3_011
  | SC_D_00_64A        => fun _ => fct3_011
  | SC_D_01_64A        => fun _ => fct3_011
  | SC_D_10_64A        => fun _ => fct3_011
  | SC_D_11_64A        => fun _ => fct3_011
  | AMOSWAP_D_00_64A   => fun _ => fct3_011
  | AMOSWAP_D_01_64A   => fun _ => fct3_011
  | AMOSWAP_D_10_64A   => fun _ => fct3_011
  | AMOSWAP_D_11_64A   => fun _ => fct3_011
  | AMOADD_D_00_64A    => fun _ => fct3_011
  | AMOADD_D_01_64A    => fun _ => fct3_011
  | AMOADD_D_10_64A    => fun _ => fct3_011
  | AMOADD_D_11_64A    => fun _ => fct3_011
  | AMOXOR_D_00_64A    => fun _ => fct3_011
  | AMOXOR_D_01_64A    => fun _ => fct3_011
  | AMOXOR_D_10_64A    => fun _ => fct3_011
  | AMOXOR_D_11_64A    => fun _ => fct3_011
  | AMOAND_D_00_64A    => fun _ => fct3_011
  | AMOAND_D_01_64A    => fun _ => fct3_011
  | AMOAND_D_10_64A    => fun _ => fct3_011
  | AMOAND_D_11_64A    => fun _ => fct3_011
  | AMOOR_D_00_64A     => fun _ => fct3_011
  | AMOOR_D_01_64A     => fun _ => fct3_011
  | AMOOR_D_10_64A     => fun _ => fct3_011
  | AMOOR_D_11_64A     => fun _ => fct3_011
  | AMOMIN_D_00_64A    => fun _ => fct3_011
  | AMOMIN_D_01_64A    => fun _ => fct3_011
  | AMOMIN_D_10_64A    => fun _ => fct3_011
  | AMOMIN_D_11_64A    => fun _ => fct3_011
  | AMOMAX_D_00_64A    => fun _ => fct3_011
  | AMOMAX_D_01_64A    => fun _ => fct3_011
  | AMOMAX_D_10_64A    => fun _ => fct3_011
  | AMOMAX_D_11_64A    => fun _ => fct3_011
  | AMOMINU_D_00_64A   => fun _ => fct3_011
  | AMOMINU_D_01_64A   => fun _ => fct3_011
  | AMOMINU_D_10_64A   => fun _ => fct3_011
  | AMOMINU_D_11_64A   => fun _ => fct3_011
  | AMOMAXU_D_00_64A   => fun _ => fct3_011
  | AMOMAXU_D_01_64A   => fun _ => fct3_011
  | AMOMAXU_D_10_64A   => fun _ => fct3_011
  | AMOMAXU_D_11_64A   => fun _ => fct3_011
  | FLW_32F            => fun _ => fct3_010
  | FSW_32F            => fun _ => fct3_010
  | FMADD_RNE_S_32F    => fun _ => fct3_000
  | FMADD_RTZ_S_32F    => fun _ => fct3_001
  | FMADD_RDN_S_32F    => fun _ => fct3_010
  | FMADD_RUP_S_32F    => fun _ => fct3_011
  | FMADD_RMM_S_32F    => fun _ => fct3_100
  | FMADD_DYN_S_32F    => fun _ => fct3_111
  | FMSUB_RNE_S_32F    => fun _ => fct3_000
  | FMSUB_RTZ_S_32F    => fun _ => fct3_001
  | FMSUB_RDN_S_32F    => fun _ => fct3_010
  | FMSUB_RUP_S_32F    => fun _ => fct3_011
  | FMSUB_RMM_S_32F    => fun _ => fct3_100
  | FMSUB_DYN_S_32F    => fun _ => fct3_111
  | FNMSUB_RNE_S_32F   => fun _ => fct3_000
  | FNMSUB_RTZ_S_32F   => fun _ => fct3_001
  | FNMSUB_RDN_S_32F   => fun _ => fct3_010
  | FNMSUB_RUP_S_32F   => fun _ => fct3_011
  | FNMSUB_RMM_S_32F   => fun _ => fct3_100
  | FNMSUB_DYN_S_32F   => fun _ => fct3_111
  | FNMADD_RNE_S_32F   => fun _ => fct3_000
  | FNMADD_RTZ_S_32F   => fun _ => fct3_001
  | FNMADD_RDN_S_32F   => fun _ => fct3_010
  | FNMADD_RUP_S_32F   => fun _ => fct3_011
  | FNMADD_RMM_S_32F   => fun _ => fct3_100
  | FNMADD_DYN_S_32F   => fun _ => fct3_111
  | FADD_RNE_S_32F     => fun _ => fct3_000
  | FADD_RTZ_S_32F     => fun _ => fct3_001
  | FADD_RDN_S_32F     => fun _ => fct3_010
  | FADD_RUP_S_32F     => fun _ => fct3_011
  | FADD_RMM_S_32F     => fun _ => fct3_100
  | FADD_DYN_S_32F     => fun _ => fct3_111
  | FSUB_RNE_S_32F     => fun _ => fct3_000
  | FSUB_RTZ_S_32F     => fun _ => fct3_001
  | FSUB_RDN_S_32F     => fun _ => fct3_010
  | FSUB_RUP_S_32F     => fun _ => fct3_011
  | FSUB_RMM_S_32F     => fun _ => fct3_100
  | FSUB_DYN_S_32F     => fun _ => fct3_111
  | FMUL_RNE_S_32F     => fun _ => fct3_000
  | FMUL_RTZ_S_32F     => fun _ => fct3_001
  | FMUL_RDN_S_32F     => fun _ => fct3_010
  | FMUL_RUP_S_32F     => fun _ => fct3_011
  | FMUL_RMM_S_32F     => fun _ => fct3_100
  | FMUL_DYN_S_32F     => fun _ => fct3_111
  | FDIV_RNE_S_32F     => fun _ => fct3_000
  | FDIV_RTZ_S_32F     => fun _ => fct3_001
  | FDIV_RDN_S_32F     => fun _ => fct3_010
  | FDIV_RUP_S_32F     => fun _ => fct3_011
  | FDIV_RMM_S_32F     => fun _ => fct3_100
  | FDIV_DYN_S_32F     => fun _ => fct3_111
  | FSQRT_RNE_S_32F    => fun _ => fct3_000
  | FSQRT_RTZ_S_32F    => fun _ => fct3_001
  | FSQRT_RDN_S_32F    => fun _ => fct3_010
  | FSQRT_RUP_S_32F    => fun _ => fct3_011
  | FSQRT_RMM_S_32F    => fun _ => fct3_100
  | FSQRT_DYN_S_32F    => fun _ => fct3_111
  | FSGNJ_S_32F        => fun _ => fct3_000
  | FSGNJN_S_32F       => fun _ => fct3_001
  | FSGNJX_S_32F       => fun _ => fct3_010
  | FMIN_S_32F         => fun _ => fct3_000
  | FMAX_S_32F         => fun _ => fct3_001
  | FCVT_RNE_W_S_32F   => fun _ => fct3_000
  | FCVT_RTZ_W_S_32F   => fun _ => fct3_001
  | FCVT_RDN_W_S_32F   => fun _ => fct3_010
  | FCVT_RUP_W_S_32F   => fun _ => fct3_011
  | FCVT_RMM_W_S_32F   => fun _ => fct3_100
  | FCVT_DYN_W_S_32F   => fun _ => fct3_111
  | FCVT_RNE_WU_S_32F  => fun _ => fct3_000
  | FCVT_RTZ_WU_S_32F  => fun _ => fct3_001
  | FCVT_RDN_WU_S_32F  => fun _ => fct3_010
  | FCVT_RUP_WU_S_32F  => fun _ => fct3_011
  | FCVT_RMM_WU_S_32F  => fun _ => fct3_100
  | FCVT_DYN_WU_S_32F  => fun _ => fct3_111
  | FMV_X_W_32F        => fun _ => fct3_000
  | FEQ_S_32F          => fun _ => fct3_010
  | FLT_S_32F          => fun _ => fct3_001
  | FLE_S_32F          => fun _ => fct3_000
  | FCLASS_S_32F       => fun _ => fct3_001
  | FCVT_RNE_S_W_32F   => fun _ => fct3_000
  | FCVT_RTZ_S_W_32F   => fun _ => fct3_001
  | FCVT_RDN_S_W_32F   => fun _ => fct3_010
  | FCVT_RUP_S_W_32F   => fun _ => fct3_011
  | FCVT_RMM_S_W_32F   => fun _ => fct3_100
  | FCVT_DYN_S_W_32F   => fun _ => fct3_111
  | FCVT_RNE_S_WU_32F  => fun _ => fct3_000
  | FCVT_RTZ_S_WU_32F  => fun _ => fct3_001
  | FCVT_RDN_S_WU_32F  => fun _ => fct3_010
  | FCVT_RUP_S_WU_32F  => fun _ => fct3_011
  | FCVT_RMM_S_WU_32F  => fun _ => fct3_100
  | FCVT_DYN_S_WU_32F  => fun _ => fct3_111
  | FMV_W_X_32F        => fun _ => fct3_000
  | FLW_64F            => fun _ => fct3_010
  | FSW_64F            => fun _ => fct3_010
  | FMADD_RNE_S_64F    => fun _ => fct3_000
  | FMADD_RTZ_S_64F    => fun _ => fct3_001
  | FMADD_RDN_S_64F    => fun _ => fct3_010
  | FMADD_RUP_S_64F    => fun _ => fct3_011
  | FMADD_RMM_S_64F    => fun _ => fct3_100
  | FMADD_DYN_S_64F    => fun _ => fct3_111
  | FMSUB_RNE_S_64F    => fun _ => fct3_000
  | FMSUB_RTZ_S_64F    => fun _ => fct3_001
  | FMSUB_RDN_S_64F    => fun _ => fct3_010
  | FMSUB_RUP_S_64F    => fun _ => fct3_011
  | FMSUB_RMM_S_64F    => fun _ => fct3_100
  | FMSUB_DYN_S_64F    => fun _ => fct3_111
  | FNMSUB_RNE_S_64F   => fun _ => fct3_000
  | FNMSUB_RTZ_S_64F   => fun _ => fct3_001
  | FNMSUB_RDN_S_64F   => fun _ => fct3_010
  | FNMSUB_RUP_S_64F   => fun _ => fct3_011
  | FNMSUB_RMM_S_64F   => fun _ => fct3_100
  | FNMSUB_DYN_S_64F   => fun _ => fct3_111
  | FNMADD_RNE_S_64F   => fun _ => fct3_000
  | FNMADD_RTZ_S_64F   => fun _ => fct3_001
  | FNMADD_RDN_S_64F   => fun _ => fct3_010
  | FNMADD_RUP_S_64F   => fun _ => fct3_011
  | FNMADD_RMM_S_64F   => fun _ => fct3_100
  | FNMADD_DYN_S_64F   => fun _ => fct3_111
  | FADD_RNE_S_64F     => fun _ => fct3_000
  | FADD_RTZ_S_64F     => fun _ => fct3_001
  | FADD_RDN_S_64F     => fun _ => fct3_010
  | FADD_RUP_S_64F     => fun _ => fct3_011
  | FADD_RMM_S_64F     => fun _ => fct3_100
  | FADD_DYN_S_64F     => fun _ => fct3_111
  | FSUB_RNE_S_64F     => fun _ => fct3_000
  | FSUB_RTZ_S_64F     => fun _ => fct3_001
  | FSUB_RDN_S_64F     => fun _ => fct3_010
  | FSUB_RUP_S_64F     => fun _ => fct3_011
  | FSUB_RMM_S_64F     => fun _ => fct3_100
  | FSUB_DYN_S_64F     => fun _ => fct3_111
  | FMUL_RNE_S_64F     => fun _ => fct3_000
  | FMUL_RTZ_S_64F     => fun _ => fct3_001
  | FMUL_RDN_S_64F     => fun _ => fct3_010
  | FMUL_RUP_S_64F     => fun _ => fct3_011
  | FMUL_RMM_S_64F     => fun _ => fct3_100
  | FMUL_DYN_S_64F     => fun _ => fct3_111
  | FDIV_RNE_S_64F     => fun _ => fct3_000
  | FDIV_RTZ_S_64F     => fun _ => fct3_001
  | FDIV_RDN_S_64F     => fun _ => fct3_010
  | FDIV_RUP_S_64F     => fun _ => fct3_011
  | FDIV_RMM_S_64F     => fun _ => fct3_100
  | FDIV_DYN_S_64F     => fun _ => fct3_111
  | FSQRT_RNE_S_64F    => fun _ => fct3_000
  | FSQRT_RTZ_S_64F    => fun _ => fct3_001
  | FSQRT_RDN_S_64F    => fun _ => fct3_010
  | FSQRT_RUP_S_64F    => fun _ => fct3_011
  | FSQRT_RMM_S_64F    => fun _ => fct3_100
  | FSQRT_DYN_S_64F    => fun _ => fct3_111
  | FSGNJ_S_64F        => fun _ => fct3_000
  | FSGNJN_S_64F       => fun _ => fct3_001
  | FSGNJX_S_64F       => fun _ => fct3_010
  | FMIN_S_64F         => fun _ => fct3_000
  | FMAX_S_64F         => fun _ => fct3_001
  | FCVT_RNE_W_S_64F   => fun _ => fct3_000
  | FCVT_RTZ_W_S_64F   => fun _ => fct3_001
  | FCVT_RDN_W_S_64F   => fun _ => fct3_010
  | FCVT_RUP_W_S_64F   => fun _ => fct3_011
  | FCVT_RMM_W_S_64F   => fun _ => fct3_100
  | FCVT_DYN_W_S_64F   => fun _ => fct3_111
  | FCVT_RNE_WU_S_64F  => fun _ => fct3_000
  | FCVT_RTZ_WU_S_64F  => fun _ => fct3_001
  | FCVT_RDN_WU_S_64F  => fun _ => fct3_010
  | FCVT_RUP_WU_S_64F  => fun _ => fct3_011
  | FCVT_RMM_WU_S_64F  => fun _ => fct3_100
  | FCVT_DYN_WU_S_64F  => fun _ => fct3_111
  | FMV_X_W_64F        => fun _ => fct3_000
  | FEQ_S_64F          => fun _ => fct3_010
  | FLT_S_64F          => fun _ => fct3_001
  | FLE_S_64F          => fun _ => fct3_000
  | FCLASS_S_64F       => fun _ => fct3_001
  | FCVT_RNE_S_W_64F   => fun _ => fct3_000
  | FCVT_RTZ_S_W_64F   => fun _ => fct3_001
  | FCVT_RDN_S_W_64F   => fun _ => fct3_010
  | FCVT_RUP_S_W_64F   => fun _ => fct3_011
  | FCVT_RMM_S_W_64F   => fun _ => fct3_100
  | FCVT_DYN_S_W_64F   => fun _ => fct3_111
  | FCVT_RNE_S_WU_64F  => fun _ => fct3_000
  | FCVT_RTZ_S_WU_64F  => fun _ => fct3_001
  | FCVT_RDN_S_WU_64F  => fun _ => fct3_010
  | FCVT_RUP_S_WU_64F  => fun _ => fct3_011
  | FCVT_RMM_S_WU_64F  => fun _ => fct3_100
  | FCVT_DYN_S_WU_64F  => fun _ => fct3_111
  | FMV_W_X_64F        => fun _ => fct3_000
  | FCVT_RNE_L_S_64F   => fun _ => fct3_000
  | FCVT_RTZ_L_S_64F   => fun _ => fct3_001
  | FCVT_RDN_L_S_64F   => fun _ => fct3_010
  | FCVT_RUP_L_S_64F   => fun _ => fct3_011
  | FCVT_RMM_L_S_64F   => fun _ => fct3_100
  | FCVT_DYN_L_S_64F   => fun _ => fct3_111
  | FCVT_RNE_LU_S_64F  => fun _ => fct3_000
  | FCVT_RTZ_LU_S_64F  => fun _ => fct3_001
  | FCVT_RDN_LU_S_64F  => fun _ => fct3_010
  | FCVT_RUP_LU_S_64F  => fun _ => fct3_011
  | FCVT_RMM_LU_S_64F  => fun _ => fct3_100
  | FCVT_DYN_LU_S_64F  => fun _ => fct3_111
  | FCVT_RNE_S_L_64F   => fun _ => fct3_000
  | FCVT_RTZ_S_L_64F   => fun _ => fct3_001
  | FCVT_RDN_S_L_64F   => fun _ => fct3_010
  | FCVT_RUP_S_L_64F   => fun _ => fct3_011
  | FCVT_RMM_S_L_64F   => fun _ => fct3_100
  | FCVT_DYN_S_L_64F   => fun _ => fct3_111
  | FCVT_RNE_S_LU_64F  => fun _ => fct3_000
  | FCVT_RTZ_S_LU_64F  => fun _ => fct3_001
  | FCVT_RDN_S_LU_64F  => fun _ => fct3_010
  | FCVT_RUP_S_LU_64F  => fun _ => fct3_011
  | FCVT_RMM_S_LU_64F  => fun _ => fct3_100
  | FCVT_DYN_S_LU_64F  => fun _ => fct3_111
  | FLD_32D            => fun _ => fct3_011
  | FSD_32D            => fun _ => fct3_011
  | FMADD_RNE_D_32D    => fun _ => fct3_000
  | FMADD_RTZ_D_32D    => fun _ => fct3_001
  | FMADD_RDN_D_32D    => fun _ => fct3_010
  | FMADD_RUP_D_32D    => fun _ => fct3_011
  | FMADD_RMM_D_32D    => fun _ => fct3_100
  | FMADD_DYN_D_32D    => fun _ => fct3_111
  | FMSUB_RNE_D_32D    => fun _ => fct3_000
  | FMSUB_RTZ_D_32D    => fun _ => fct3_001
  | FMSUB_RDN_D_32D    => fun _ => fct3_010
  | FMSUB_RUP_D_32D    => fun _ => fct3_011
  | FMSUB_RMM_D_32D    => fun _ => fct3_100
  | FMSUB_DYN_D_32D    => fun _ => fct3_111
  | FNMSUB_RNE_D_32D   => fun _ => fct3_000
  | FNMSUB_RTZ_D_32D   => fun _ => fct3_001
  | FNMSUB_RDN_D_32D   => fun _ => fct3_010
  | FNMSUB_RUP_D_32D   => fun _ => fct3_011
  | FNMSUB_RMM_D_32D   => fun _ => fct3_100
  | FNMSUB_DYN_D_32D   => fun _ => fct3_111
  | FNMADD_RNE_D_32D   => fun _ => fct3_000
  | FNMADD_RTZ_D_32D   => fun _ => fct3_001
  | FNMADD_RDN_D_32D   => fun _ => fct3_010
  | FNMADD_RUP_D_32D   => fun _ => fct3_011
  | FNMADD_RMM_D_32D   => fun _ => fct3_100
  | FNMADD_DYN_D_32D   => fun _ => fct3_111
  | FADD_RNE_D_32D     => fun _ => fct3_000
  | FADD_RTZ_D_32D     => fun _ => fct3_001
  | FADD_RDN_D_32D     => fun _ => fct3_010
  | FADD_RUP_D_32D     => fun _ => fct3_011
  | FADD_RMM_D_32D     => fun _ => fct3_100
  | FADD_DYN_D_32D     => fun _ => fct3_111
  | FSUB_RNE_D_32D     => fun _ => fct3_000
  | FSUB_RTZ_D_32D     => fun _ => fct3_001
  | FSUB_RDN_D_32D     => fun _ => fct3_010
  | FSUB_RUP_D_32D     => fun _ => fct3_011
  | FSUB_RMM_D_32D     => fun _ => fct3_100
  | FSUB_DYN_D_32D     => fun _ => fct3_111
  | FMUL_RNE_D_32D     => fun _ => fct3_000
  | FMUL_RTZ_D_32D     => fun _ => fct3_001
  | FMUL_RDN_D_32D     => fun _ => fct3_010
  | FMUL_RUP_D_32D     => fun _ => fct3_011
  | FMUL_RMM_D_32D     => fun _ => fct3_100
  | FMUL_DYN_D_32D     => fun _ => fct3_111
  | FDIV_RNE_D_32D     => fun _ => fct3_000
  | FDIV_RTZ_D_32D     => fun _ => fct3_001
  | FDIV_RDN_D_32D     => fun _ => fct3_010
  | FDIV_RUP_D_32D     => fun _ => fct3_011
  | FDIV_RMM_D_32D     => fun _ => fct3_100
  | FDIV_DYN_D_32D     => fun _ => fct3_111
  | FSQRT_RNE_D_32D    => fun _ => fct3_000
  | FSQRT_RTZ_D_32D    => fun _ => fct3_001
  | FSQRT_RDN_D_32D    => fun _ => fct3_010
  | FSQRT_RUP_D_32D    => fun _ => fct3_011
  | FSQRT_RMM_D_32D    => fun _ => fct3_100
  | FSQRT_DYN_D_32D    => fun _ => fct3_111
  | FSGNJ_D_32D        => fun _ => fct3_000
  | FSGNJN_D_32D       => fun _ => fct3_001
  | FSGNJX_D_32D       => fun _ => fct3_010
  | FMIN_D_32D         => fun _ => fct3_000
  | FMAX_D_32D         => fun _ => fct3_001
  | FCVT_RNE_S_D_32D   => fun _ => fct3_000
  | FCVT_RTZ_S_D_32D   => fun _ => fct3_001
  | FCVT_RDN_S_D_32D   => fun _ => fct3_010
  | FCVT_RUP_S_D_32D   => fun _ => fct3_011
  | FCVT_RMM_S_D_32D   => fun _ => fct3_100
  | FCVT_DYN_S_D_32D   => fun _ => fct3_111
  | FCVT_RNE_D_S_32D   => fun _ => fct3_000
  | FCVT_RTZ_D_S_32D   => fun _ => fct3_001
  | FCVT_RDN_D_S_32D   => fun _ => fct3_010
  | FCVT_RUP_D_S_32D   => fun _ => fct3_011
  | FCVT_RMM_D_S_32D   => fun _ => fct3_100
  | FCVT_DYN_D_S_32D   => fun _ => fct3_111
  | FEQ_D_32D          => fun _ => fct3_010
  | FLT_D_32D          => fun _ => fct3_001
  | FLE_D_32D          => fun _ => fct3_000
  | FCLASS_D_32D       => fun _ => fct3_001
  | FCVT_RNE_W_D_32D   => fun _ => fct3_000
  | FCVT_RTZ_W_D_32D   => fun _ => fct3_001
  | FCVT_RDN_W_D_32D   => fun _ => fct3_010
  | FCVT_RUP_W_D_32D   => fun _ => fct3_011
  | FCVT_RMM_W_D_32D   => fun _ => fct3_100
  | FCVT_DYN_W_D_32D   => fun _ => fct3_111
  | FCVT_RNE_WU_D_32D  => fun _ => fct3_000
  | FCVT_RTZ_WU_D_32D  => fun _ => fct3_001
  | FCVT_RDN_WU_D_32D  => fun _ => fct3_010
  | FCVT_RUP_WU_D_32D  => fun _ => fct3_011
  | FCVT_RMM_WU_D_32D  => fun _ => fct3_100
  | FCVT_DYN_WU_D_32D  => fun _ => fct3_111
  | FCVT_RNE_D_W_32D   => fun _ => fct3_000
  | FCVT_RTZ_D_W_32D   => fun _ => fct3_001
  | FCVT_RDN_D_W_32D   => fun _ => fct3_010
  | FCVT_RUP_D_W_32D   => fun _ => fct3_011
  | FCVT_RMM_D_W_32D   => fun _ => fct3_100
  | FCVT_DYN_D_W_32D   => fun _ => fct3_111
  | FCVT_RNE_D_WU_32D  => fun _ => fct3_000
  | FCVT_RTZ_D_WU_32D  => fun _ => fct3_001
  | FCVT_RDN_D_WU_32D  => fun _ => fct3_010
  | FCVT_RUP_D_WU_32D  => fun _ => fct3_011
  | FCVT_RMM_D_WU_32D  => fun _ => fct3_100
  | FCVT_DYN_D_WU_32D  => fun _ => fct3_111
  | FLD_64D            => fun _ => fct3_011
  | FSD_64D            => fun _ => fct3_011
  | FMADD_RNE_D_64D    => fun _ => fct3_000
  | FMADD_RTZ_D_64D    => fun _ => fct3_001
  | FMADD_RDN_D_64D    => fun _ => fct3_010
  | FMADD_RUP_D_64D    => fun _ => fct3_011
  | FMADD_RMM_D_64D    => fun _ => fct3_100
  | FMADD_DYN_D_64D    => fun _ => fct3_111
  | FMSUB_RNE_D_64D    => fun _ => fct3_000
  | FMSUB_RTZ_D_64D    => fun _ => fct3_001
  | FMSUB_RDN_D_64D    => fun _ => fct3_010
  | FMSUB_RUP_D_64D    => fun _ => fct3_011
  | FMSUB_RMM_D_64D    => fun _ => fct3_100
  | FMSUB_DYN_D_64D    => fun _ => fct3_111
  | FNMSUB_RNE_D_64D   => fun _ => fct3_000
  | FNMSUB_RTZ_D_64D   => fun _ => fct3_001
  | FNMSUB_RDN_D_64D   => fun _ => fct3_010
  | FNMSUB_RUP_D_64D   => fun _ => fct3_011
  | FNMSUB_RMM_D_64D   => fun _ => fct3_100
  | FNMSUB_DYN_D_64D   => fun _ => fct3_111
  | FNMADD_RNE_D_64D   => fun _ => fct3_000
  | FNMADD_RTZ_D_64D   => fun _ => fct3_001
  | FNMADD_RDN_D_64D   => fun _ => fct3_010
  | FNMADD_RUP_D_64D   => fun _ => fct3_011
  | FNMADD_RMM_D_64D   => fun _ => fct3_100
  | FNMADD_DYN_D_64D   => fun _ => fct3_111
  | FADD_RNE_D_64D     => fun _ => fct3_000
  | FADD_RTZ_D_64D     => fun _ => fct3_001
  | FADD_RDN_D_64D     => fun _ => fct3_010
  | FADD_RUP_D_64D     => fun _ => fct3_011
  | FADD_RMM_D_64D     => fun _ => fct3_100
  | FADD_DYN_D_64D     => fun _ => fct3_111
  | FSUB_RNE_D_64D     => fun _ => fct3_000
  | FSUB_RTZ_D_64D     => fun _ => fct3_001
  | FSUB_RDN_D_64D     => fun _ => fct3_010
  | FSUB_RUP_D_64D     => fun _ => fct3_011
  | FSUB_RMM_D_64D     => fun _ => fct3_100
  | FSUB_DYN_D_64D     => fun _ => fct3_111
  | FMUL_RNE_D_64D     => fun _ => fct3_000
  | FMUL_RTZ_D_64D     => fun _ => fct3_001
  | FMUL_RDN_D_64D     => fun _ => fct3_010
  | FMUL_RUP_D_64D     => fun _ => fct3_011
  | FMUL_RMM_D_64D     => fun _ => fct3_100
  | FMUL_DYN_D_64D     => fun _ => fct3_111
  | FDIV_RNE_D_64D     => fun _ => fct3_000
  | FDIV_RTZ_D_64D     => fun _ => fct3_001
  | FDIV_RDN_D_64D     => fun _ => fct3_010
  | FDIV_RUP_D_64D     => fun _ => fct3_011
  | FDIV_RMM_D_64D     => fun _ => fct3_100
  | FDIV_DYN_D_64D     => fun _ => fct3_111
  | FSQRT_RNE_D_64D    => fun _ => fct3_000
  | FSQRT_RTZ_D_64D    => fun _ => fct3_001
  | FSQRT_RDN_D_64D    => fun _ => fct3_010
  | FSQRT_RUP_D_64D    => fun _ => fct3_011
  | FSQRT_RMM_D_64D    => fun _ => fct3_100
  | FSQRT_DYN_D_64D    => fun _ => fct3_111
  | FSGNJ_D_64D        => fun _ => fct3_000
  | FSGNJN_D_64D       => fun _ => fct3_001
  | FSGNJX_D_64D       => fun _ => fct3_010
  | FMIN_D_64D         => fun _ => fct3_000
  | FMAX_D_64D         => fun _ => fct3_001
  | FCVT_RNE_S_D_64D   => fun _ => fct3_000
  | FCVT_RTZ_S_D_64D   => fun _ => fct3_001
  | FCVT_RDN_S_D_64D   => fun _ => fct3_010
  | FCVT_RUP_S_D_64D   => fun _ => fct3_011
  | FCVT_RMM_S_D_64D   => fun _ => fct3_100
  | FCVT_DYN_S_D_64D   => fun _ => fct3_111
  | FCVT_RNE_D_S_64D   => fun _ => fct3_000
  | FCVT_RTZ_D_S_64D   => fun _ => fct3_001
  | FCVT_RDN_D_S_64D   => fun _ => fct3_010
  | FCVT_RUP_D_S_64D   => fun _ => fct3_011
  | FCVT_RMM_D_S_64D   => fun _ => fct3_100
  | FCVT_DYN_D_S_64D   => fun _ => fct3_111
  | FEQ_D_64D          => fun _ => fct3_010
  | FLT_D_64D          => fun _ => fct3_001
  | FLE_D_64D          => fun _ => fct3_000
  | FCLASS_D_64D       => fun _ => fct3_001
  | FCVT_RNE_W_D_64D   => fun _ => fct3_000
  | FCVT_RTZ_W_D_64D   => fun _ => fct3_001
  | FCVT_RDN_W_D_64D   => fun _ => fct3_010
  | FCVT_RUP_W_D_64D   => fun _ => fct3_011
  | FCVT_RMM_W_D_64D   => fun _ => fct3_100
  | FCVT_DYN_W_D_64D   => fun _ => fct3_111
  | FCVT_RNE_WU_D_64D  => fun _ => fct3_000
  | FCVT_RTZ_WU_D_64D  => fun _ => fct3_001
  | FCVT_RDN_WU_D_64D  => fun _ => fct3_010
  | FCVT_RUP_WU_D_64D  => fun _ => fct3_011
  | FCVT_RMM_WU_D_64D  => fun _ => fct3_100
  | FCVT_DYN_WU_D_64D  => fun _ => fct3_111
  | FCVT_RNE_D_W_64D   => fun _ => fct3_000
  | FCVT_RTZ_D_W_64D   => fun _ => fct3_001
  | FCVT_RDN_D_W_64D   => fun _ => fct3_010
  | FCVT_RUP_D_W_64D   => fun _ => fct3_011
  | FCVT_RMM_D_W_64D   => fun _ => fct3_100
  | FCVT_DYN_D_W_64D   => fun _ => fct3_111
  | FCVT_RNE_D_WU_64D  => fun _ => fct3_000
  | FCVT_RTZ_D_WU_64D  => fun _ => fct3_001
  | FCVT_RDN_D_WU_64D  => fun _ => fct3_010
  | FCVT_RUP_D_WU_64D  => fun _ => fct3_011
  | FCVT_RMM_D_WU_64D  => fun _ => fct3_100
  | FCVT_DYN_D_WU_64D  => fun _ => fct3_111
  | FCVT_RNE_L_D_64D   => fun _ => fct3_000
  | FCVT_RTZ_L_D_64D   => fun _ => fct3_001
  | FCVT_RDN_L_D_64D   => fun _ => fct3_010
  | FCVT_RUP_L_D_64D   => fun _ => fct3_011
  | FCVT_RMM_L_D_64D   => fun _ => fct3_100
  | FCVT_DYN_L_D_64D   => fun _ => fct3_111
  | FCVT_RNE_LU_D_64D  => fun _ => fct3_000
  | FCVT_RTZ_LU_D_64D  => fun _ => fct3_001
  | FCVT_RDN_LU_D_64D  => fun _ => fct3_010
  | FCVT_RUP_LU_D_64D  => fun _ => fct3_011
  | FCVT_RMM_LU_D_64D  => fun _ => fct3_100
  | FCVT_DYN_LU_D_64D  => fun _ => fct3_111
  | FMV_X_D_64D        => fun _ => fct3_000
  | FCVT_RNE_D_L_64D   => fun _ => fct3_000
  | FCVT_RTZ_D_L_64D   => fun _ => fct3_001
  | FCVT_RDN_D_L_64D   => fun _ => fct3_010
  | FCVT_RUP_D_L_64D   => fun _ => fct3_011
  | FCVT_RMM_D_L_64D   => fun _ => fct3_100
  | FCVT_DYN_D_L_64D   => fun _ => fct3_111
  | FCVT_RNE_D_LU_64D  => fun _ => fct3_000
  | FCVT_RTZ_D_LU_64D  => fun _ => fct3_001
  | FCVT_RDN_D_LU_64D  => fun _ => fct3_010
  | FCVT_RUP_D_LU_64D  => fun _ => fct3_011
  | FCVT_RMM_D_LU_64D  => fun _ => fct3_100
  | FCVT_DYN_D_LU_64D  => fun _ => fct3_111
  | FMV_D_X_64D        => fun _ => fct3_000
  | FLQ_32Q            => fun _ => fct3_100
  | FSQ_32Q            => fun _ => fct3_100
  | FMADD_RNE_Q_32Q    => fun _ => fct3_000
  | FMADD_RTZ_Q_32Q    => fun _ => fct3_001
  | FMADD_RDN_Q_32Q    => fun _ => fct3_010
  | FMADD_RUP_Q_32Q    => fun _ => fct3_011
  | FMADD_RMM_Q_32Q    => fun _ => fct3_100
  | FMADD_DYN_Q_32Q    => fun _ => fct3_111
  | FMSUB_RNE_Q_32Q    => fun _ => fct3_000
  | FMSUB_RTZ_Q_32Q    => fun _ => fct3_001
  | FMSUB_RDN_Q_32Q    => fun _ => fct3_010
  | FMSUB_RUP_Q_32Q    => fun _ => fct3_011
  | FMSUB_RMM_Q_32Q    => fun _ => fct3_100
  | FMSUB_DYN_Q_32Q    => fun _ => fct3_111
  | FNMSUB_RNE_Q_32Q   => fun _ => fct3_000
  | FNMSUB_RTZ_Q_32Q   => fun _ => fct3_001
  | FNMSUB_RDN_Q_32Q   => fun _ => fct3_010
  | FNMSUB_RUP_Q_32Q   => fun _ => fct3_011
  | FNMSUB_RMM_Q_32Q   => fun _ => fct3_100
  | FNMSUB_DYN_Q_32Q   => fun _ => fct3_111
  | FNMADD_RNE_Q_32Q   => fun _ => fct3_000
  | FNMADD_RTZ_Q_32Q   => fun _ => fct3_001
  | FNMADD_RDN_Q_32Q   => fun _ => fct3_010
  | FNMADD_RUP_Q_32Q   => fun _ => fct3_011
  | FNMADD_RMM_Q_32Q   => fun _ => fct3_100
  | FNMADD_DYN_Q_32Q   => fun _ => fct3_111
  | FADD_RNE_Q_32Q     => fun _ => fct3_000
  | FADD_RTZ_Q_32Q     => fun _ => fct3_001
  | FADD_RDN_Q_32Q     => fun _ => fct3_010
  | FADD_RUP_Q_32Q     => fun _ => fct3_011
  | FADD_RMM_Q_32Q     => fun _ => fct3_100
  | FADD_DYN_Q_32Q     => fun _ => fct3_111
  | FSUB_RNE_Q_32Q     => fun _ => fct3_000
  | FSUB_RTZ_Q_32Q     => fun _ => fct3_001
  | FSUB_RDN_Q_32Q     => fun _ => fct3_010
  | FSUB_RUP_Q_32Q     => fun _ => fct3_011
  | FSUB_RMM_Q_32Q     => fun _ => fct3_100
  | FSUB_DYN_Q_32Q     => fun _ => fct3_111
  | FMUL_RNE_Q_32Q     => fun _ => fct3_000
  | FMUL_RTZ_Q_32Q     => fun _ => fct3_001
  | FMUL_RDN_Q_32Q     => fun _ => fct3_010
  | FMUL_RUP_Q_32Q     => fun _ => fct3_011
  | FMUL_RMM_Q_32Q     => fun _ => fct3_100
  | FMUL_DYN_Q_32Q     => fun _ => fct3_111
  | FDIV_RNE_Q_32Q     => fun _ => fct3_000
  | FDIV_RTZ_Q_32Q     => fun _ => fct3_001
  | FDIV_RDN_Q_32Q     => fun _ => fct3_010
  | FDIV_RUP_Q_32Q     => fun _ => fct3_011
  | FDIV_RMM_Q_32Q     => fun _ => fct3_100
  | FDIV_DYN_Q_32Q     => fun _ => fct3_111
  | FSQRT_RNE_Q_32Q    => fun _ => fct3_000
  | FSQRT_RTZ_Q_32Q    => fun _ => fct3_001
  | FSQRT_RDN_Q_32Q    => fun _ => fct3_010
  | FSQRT_RUP_Q_32Q    => fun _ => fct3_011
  | FSQRT_RMM_Q_32Q    => fun _ => fct3_100
  | FSQRT_DYN_Q_32Q    => fun _ => fct3_111
  | FSGNJ_Q_32Q        => fun _ => fct3_000
  | FSGNJN_Q_32Q       => fun _ => fct3_001
  | FSGNJX_Q_32Q       => fun _ => fct3_010
  | FMIN_Q_32Q         => fun _ => fct3_000
  | FMAX_Q_32Q         => fun _ => fct3_001
  | FCVT_RNE_S_Q_32Q   => fun _ => fct3_000
  | FCVT_RTZ_S_Q_32Q   => fun _ => fct3_001
  | FCVT_RDN_S_Q_32Q   => fun _ => fct3_010
  | FCVT_RUP_S_Q_32Q   => fun _ => fct3_011
  | FCVT_RMM_S_Q_32Q   => fun _ => fct3_100
  | FCVT_DYN_S_Q_32Q   => fun _ => fct3_111
  | FCVT_RNE_Q_S_32Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_S_32Q   => fun _ => fct3_001
  | FCVT_RDN_Q_S_32Q   => fun _ => fct3_010
  | FCVT_RUP_Q_S_32Q   => fun _ => fct3_011
  | FCVT_RMM_Q_S_32Q   => fun _ => fct3_100
  | FCVT_DYN_Q_S_32Q   => fun _ => fct3_111
  | FCVT_RNE_D_Q_32Q   => fun _ => fct3_000
  | FCVT_RTZ_D_Q_32Q   => fun _ => fct3_001
  | FCVT_RDN_D_Q_32Q   => fun _ => fct3_010
  | FCVT_RUP_D_Q_32Q   => fun _ => fct3_011
  | FCVT_RMM_D_Q_32Q   => fun _ => fct3_100
  | FCVT_DYN_D_Q_32Q   => fun _ => fct3_111
  | FCVT_RNE_Q_D_32Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_D_32Q   => fun _ => fct3_001
  | FCVT_RDN_Q_D_32Q   => fun _ => fct3_010
  | FCVT_RUP_Q_D_32Q   => fun _ => fct3_011
  | FCVT_RMM_Q_D_32Q   => fun _ => fct3_100
  | FCVT_DYN_Q_D_32Q   => fun _ => fct3_111
  | FEQ_Q_32Q          => fun _ => fct3_010
  | FLT_Q_32Q          => fun _ => fct3_001
  | FLE_Q_32Q          => fun _ => fct3_000
  | FCLASS_Q_32Q       => fun _ => fct3_001
  | FCVT_RNE_W_Q_32Q   => fun _ => fct3_000
  | FCVT_RTZ_W_Q_32Q   => fun _ => fct3_001
  | FCVT_RDN_W_Q_32Q   => fun _ => fct3_010
  | FCVT_RUP_W_Q_32Q   => fun _ => fct3_011
  | FCVT_RMM_W_Q_32Q   => fun _ => fct3_100
  | FCVT_DYN_W_Q_32Q   => fun _ => fct3_111
  | FCVT_RNE_WU_Q_32Q  => fun _ => fct3_000
  | FCVT_RTZ_WU_Q_32Q  => fun _ => fct3_001
  | FCVT_RDN_WU_Q_32Q  => fun _ => fct3_010
  | FCVT_RUP_WU_Q_32Q  => fun _ => fct3_011
  | FCVT_RMM_WU_Q_32Q  => fun _ => fct3_100
  | FCVT_DYN_WU_Q_32Q  => fun _ => fct3_111
  | FCVT_RNE_Q_W_32Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_W_32Q   => fun _ => fct3_001
  | FCVT_RDN_Q_W_32Q   => fun _ => fct3_010
  | FCVT_RUP_Q_W_32Q   => fun _ => fct3_011
  | FCVT_RMM_Q_W_32Q   => fun _ => fct3_100
  | FCVT_DYN_Q_W_32Q   => fun _ => fct3_111
  | FCVT_RNE_Q_WU_32Q  => fun _ => fct3_000
  | FCVT_RTZ_Q_WU_32Q  => fun _ => fct3_001
  | FCVT_RDN_Q_WU_32Q  => fun _ => fct3_010
  | FCVT_RUP_Q_WU_32Q  => fun _ => fct3_011
  | FCVT_RMM_Q_WU_32Q  => fun _ => fct3_100
  | FCVT_DYN_Q_WU_32Q  => fun _ => fct3_111
  | FLQ_64Q            => fun _ => fct3_100
  | FSQ_64Q            => fun _ => fct3_100
  | FMADD_RNE_Q_64Q    => fun _ => fct3_000
  | FMADD_RTZ_Q_64Q    => fun _ => fct3_001
  | FMADD_RDN_Q_64Q    => fun _ => fct3_010
  | FMADD_RUP_Q_64Q    => fun _ => fct3_011
  | FMADD_RMM_Q_64Q    => fun _ => fct3_100
  | FMADD_DYN_Q_64Q    => fun _ => fct3_111
  | FMSUB_RNE_Q_64Q    => fun _ => fct3_000
  | FMSUB_RTZ_Q_64Q    => fun _ => fct3_001
  | FMSUB_RDN_Q_64Q    => fun _ => fct3_010
  | FMSUB_RUP_Q_64Q    => fun _ => fct3_011
  | FMSUB_RMM_Q_64Q    => fun _ => fct3_100
  | FMSUB_DYN_Q_64Q    => fun _ => fct3_111
  | FNMSUB_RNE_Q_64Q   => fun _ => fct3_000
  | FNMSUB_RTZ_Q_64Q   => fun _ => fct3_001
  | FNMSUB_RDN_Q_64Q   => fun _ => fct3_010
  | FNMSUB_RUP_Q_64Q   => fun _ => fct3_011
  | FNMSUB_RMM_Q_64Q   => fun _ => fct3_100
  | FNMSUB_DYN_Q_64Q   => fun _ => fct3_111
  | FNMADD_RNE_Q_64Q   => fun _ => fct3_000
  | FNMADD_RTZ_Q_64Q   => fun _ => fct3_001
  | FNMADD_RDN_Q_64Q   => fun _ => fct3_010
  | FNMADD_RUP_Q_64Q   => fun _ => fct3_011
  | FNMADD_RMM_Q_64Q   => fun _ => fct3_100
  | FNMADD_DYN_Q_64Q   => fun _ => fct3_111
  | FADD_RNE_Q_64Q     => fun _ => fct3_000
  | FADD_RTZ_Q_64Q     => fun _ => fct3_001
  | FADD_RDN_Q_64Q     => fun _ => fct3_010
  | FADD_RUP_Q_64Q     => fun _ => fct3_011
  | FADD_RMM_Q_64Q     => fun _ => fct3_100
  | FADD_DYN_Q_64Q     => fun _ => fct3_111
  | FSUB_RNE_Q_64Q     => fun _ => fct3_000
  | FSUB_RTZ_Q_64Q     => fun _ => fct3_001
  | FSUB_RDN_Q_64Q     => fun _ => fct3_010
  | FSUB_RUP_Q_64Q     => fun _ => fct3_011
  | FSUB_RMM_Q_64Q     => fun _ => fct3_100
  | FSUB_DYN_Q_64Q     => fun _ => fct3_111
  | FMUL_RNE_Q_64Q     => fun _ => fct3_000
  | FMUL_RTZ_Q_64Q     => fun _ => fct3_001
  | FMUL_RDN_Q_64Q     => fun _ => fct3_010
  | FMUL_RUP_Q_64Q     => fun _ => fct3_011
  | FMUL_RMM_Q_64Q     => fun _ => fct3_100
  | FMUL_DYN_Q_64Q     => fun _ => fct3_111
  | FDIV_RNE_Q_64Q     => fun _ => fct3_000
  | FDIV_RTZ_Q_64Q     => fun _ => fct3_001
  | FDIV_RDN_Q_64Q     => fun _ => fct3_010
  | FDIV_RUP_Q_64Q     => fun _ => fct3_011
  | FDIV_RMM_Q_64Q     => fun _ => fct3_100
  | FDIV_DYN_Q_64Q     => fun _ => fct3_111
  | FSQRT_RNE_Q_64Q    => fun _ => fct3_000
  | FSQRT_RTZ_Q_64Q    => fun _ => fct3_001
  | FSQRT_RDN_Q_64Q    => fun _ => fct3_010
  | FSQRT_RUP_Q_64Q    => fun _ => fct3_011
  | FSQRT_RMM_Q_64Q    => fun _ => fct3_100
  | FSQRT_DYN_Q_64Q    => fun _ => fct3_111
  | FSGNJ_Q_64Q        => fun _ => fct3_000
  | FSGNJN_Q_64Q       => fun _ => fct3_001
  | FSGNJX_Q_64Q       => fun _ => fct3_010
  | FMIN_Q_64Q         => fun _ => fct3_000
  | FMAX_Q_64Q         => fun _ => fct3_001
  | FCVT_RNE_S_Q_64Q   => fun _ => fct3_000
  | FCVT_RTZ_S_Q_64Q   => fun _ => fct3_001
  | FCVT_RDN_S_Q_64Q   => fun _ => fct3_010
  | FCVT_RUP_S_Q_64Q   => fun _ => fct3_011
  | FCVT_RMM_S_Q_64Q   => fun _ => fct3_100
  | FCVT_DYN_S_Q_64Q   => fun _ => fct3_111
  | FCVT_RNE_Q_S_64Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_S_64Q   => fun _ => fct3_001
  | FCVT_RDN_Q_S_64Q   => fun _ => fct3_010
  | FCVT_RUP_Q_S_64Q   => fun _ => fct3_011
  | FCVT_RMM_Q_S_64Q   => fun _ => fct3_100
  | FCVT_DYN_Q_S_64Q   => fun _ => fct3_111
  | FCVT_RNE_D_Q_64Q   => fun _ => fct3_000
  | FCVT_RTZ_D_Q_64Q   => fun _ => fct3_001
  | FCVT_RDN_D_Q_64Q   => fun _ => fct3_010
  | FCVT_RUP_D_Q_64Q   => fun _ => fct3_011
  | FCVT_RMM_D_Q_64Q   => fun _ => fct3_100
  | FCVT_DYN_D_Q_64Q   => fun _ => fct3_111
  | FCVT_RNE_Q_D_64Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_D_64Q   => fun _ => fct3_001
  | FCVT_RDN_Q_D_64Q   => fun _ => fct3_010
  | FCVT_RUP_Q_D_64Q   => fun _ => fct3_011
  | FCVT_RMM_Q_D_64Q   => fun _ => fct3_100
  | FCVT_DYN_Q_D_64Q   => fun _ => fct3_111
  | FEQ_Q_64Q          => fun _ => fct3_010
  | FLT_Q_64Q          => fun _ => fct3_001
  | FLE_Q_64Q          => fun _ => fct3_000
  | FCLASS_Q_64Q       => fun _ => fct3_001
  | FCVT_RNE_W_Q_64Q   => fun _ => fct3_000
  | FCVT_RTZ_W_Q_64Q   => fun _ => fct3_001
  | FCVT_RDN_W_Q_64Q   => fun _ => fct3_010
  | FCVT_RUP_W_Q_64Q   => fun _ => fct3_011
  | FCVT_RMM_W_Q_64Q   => fun _ => fct3_100
  | FCVT_DYN_W_Q_64Q   => fun _ => fct3_111
  | FCVT_RNE_WU_Q_64Q  => fun _ => fct3_000
  | FCVT_RTZ_WU_Q_64Q  => fun _ => fct3_001
  | FCVT_RDN_WU_Q_64Q  => fun _ => fct3_010
  | FCVT_RUP_WU_Q_64Q  => fun _ => fct3_011
  | FCVT_RMM_WU_Q_64Q  => fun _ => fct3_100
  | FCVT_DYN_WU_Q_64Q  => fun _ => fct3_111
  | FCVT_RNE_Q_W_64Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_W_64Q   => fun _ => fct3_001
  | FCVT_RDN_Q_W_64Q   => fun _ => fct3_010
  | FCVT_RUP_Q_W_64Q   => fun _ => fct3_011
  | FCVT_RMM_Q_W_64Q   => fun _ => fct3_100
  | FCVT_DYN_Q_W_64Q   => fun _ => fct3_111
  | FCVT_RNE_Q_WU_64Q  => fun _ => fct3_000
  | FCVT_RTZ_Q_WU_64Q  => fun _ => fct3_001
  | FCVT_RDN_Q_WU_64Q  => fun _ => fct3_010
  | FCVT_RUP_Q_WU_64Q  => fun _ => fct3_011
  | FCVT_RMM_Q_WU_64Q  => fun _ => fct3_100
  | FCVT_DYN_Q_WU_64Q  => fun _ => fct3_111
  | FCVT_RNE_L_Q_64Q   => fun _ => fct3_000
  | FCVT_RTZ_L_Q_64Q   => fun _ => fct3_001
  | FCVT_RDN_L_Q_64Q   => fun _ => fct3_010
  | FCVT_RUP_L_Q_64Q   => fun _ => fct3_011
  | FCVT_RMM_L_Q_64Q   => fun _ => fct3_100
  | FCVT_DYN_L_Q_64Q   => fun _ => fct3_111
  | FCVT_RNE_LU_Q_64Q  => fun _ => fct3_000
  | FCVT_RTZ_LU_Q_64Q  => fun _ => fct3_001
  | FCVT_RDN_LU_Q_64Q  => fun _ => fct3_010
  | FCVT_RUP_LU_Q_64Q  => fun _ => fct3_011
  | FCVT_RMM_LU_Q_64Q  => fun _ => fct3_100
  | FCVT_DYN_LU_Q_64Q  => fun _ => fct3_111
  | FCVT_RNE_Q_L_64Q   => fun _ => fct3_000
  | FCVT_RTZ_Q_L_64Q   => fun _ => fct3_001
  | FCVT_RDN_Q_L_64Q   => fun _ => fct3_010
  | FCVT_RUP_Q_L_64Q   => fun _ => fct3_011
  | FCVT_RMM_Q_L_64Q   => fun _ => fct3_100
  | FCVT_DYN_Q_L_64Q   => fun _ => fct3_111
  | FCVT_RNE_Q_LU_64Q  => fun _ => fct3_000
  | FCVT_RTZ_Q_LU_64Q  => fun _ => fct3_001
  | FCVT_RDN_Q_LU_64Q  => fun _ => fct3_010
  | FCVT_RUP_Q_LU_64Q  => fun _ => fct3_011
  | FCVT_RMM_Q_LU_64Q  => fun _ => fct3_100
  | FCVT_DYN_Q_LU_64Q  => fun _ => fct3_111
  | _                  => fun _ => False_rec _ _
  end); try reflexivity; simpl in e; inversion e.
Defined.

Definition maybe_instruction_fct3 (i : instruction) : option fct3_type :=
  match i with
  | JALR_32I           => Some fct3_000
  | BEQ_32I            => Some fct3_000
  | BNE_32I            => Some fct3_001
  | BLT_32I            => Some fct3_100
  | BGE_32I            => Some fct3_101
  | BLTU_32I           => Some fct3_110
  | BGEU_32I           => Some fct3_111
  | LB_32I             => Some fct3_000
  | LH_32I             => Some fct3_001
  | LW_32I             => Some fct3_010
  | LBU_32I            => Some fct3_100
  | LHU_32I            => Some fct3_101
  | SB_32I             => Some fct3_000
  | SH_32I             => Some fct3_001
  | SW_32I             => Some fct3_010
  | ADDI_32I           => Some fct3_000
  | SLTI_32I           => Some fct3_010
  | SLTIU_32I          => Some fct3_011
  | XORI_32I           => Some fct3_100
  | ORI_32I            => Some fct3_110
  | ANDI_32I           => Some fct3_111
  | SLLI_32I           => Some fct3_001
  | SRLI_32I           => Some fct3_101
  | SRAI_32I           => Some fct3_101
  | ADD_32I            => Some fct3_000
  | SUB_32I            => Some fct3_000
  | SLL_32I            => Some fct3_001
  | SLT_32I            => Some fct3_010
  | SLTU_32I           => Some fct3_011
  | XOR_32I            => Some fct3_100
  | SRL_32I            => Some fct3_101
  | SRA_32I            => Some fct3_101
  | OR_32I             => Some fct3_110
  | AND_32I            => Some fct3_111
  | FENCE_32I          => Some fct3_000
  | ECALL_32I          => Some fct3_000
  | EBREAK_32I         => Some fct3_000
  | JALR_64I           => Some fct3_000
  | BEQ_64I            => Some fct3_000
  | BNE_64I            => Some fct3_001
  | BLT_64I            => Some fct3_100
  | BGE_64I            => Some fct3_101
  | BLTU_64I           => Some fct3_110
  | BGEU_64I           => Some fct3_111
  | LB_64I             => Some fct3_000
  | LH_64I             => Some fct3_001
  | LW_64I             => Some fct3_010
  | LBU_64I            => Some fct3_100
  | LHU_64I            => Some fct3_101
  | SB_64I             => Some fct3_000
  | SH_64I             => Some fct3_001
  | SW_64I             => Some fct3_010
  | ADDI_64I           => Some fct3_000
  | SLTI_64I           => Some fct3_010
  | SLTIU_64I          => Some fct3_011
  | XORI_64I           => Some fct3_100
  | ORI_64I            => Some fct3_110
  | ANDI_64I           => Some fct3_111
  | SLLI_64I           => Some fct3_001
  | SRLI_64I           => Some fct3_101
  | SRAI_64I           => Some fct3_101
  | ADD_64I            => Some fct3_000
  | SUB_64I            => Some fct3_000
  | SLL_64I            => Some fct3_001
  | SLT_64I            => Some fct3_010
  | SLTU_64I           => Some fct3_011
  | XOR_64I            => Some fct3_100
  | SRL_64I            => Some fct3_101
  | SRA_64I            => Some fct3_101
  | OR_64I             => Some fct3_110
  | AND_64I            => Some fct3_111
  | FENCE_64I          => Some fct3_000
  | ECALL_64I          => Some fct3_000
  | EBREAK_64I         => Some fct3_000
  | LWU_64I            => Some fct3_110
  | LD_64I             => Some fct3_011
  | SD_64I             => Some fct3_011
  | ADDIW_64I          => Some fct3_000
  | SLLIW_64I          => Some fct3_001
  | SRLIW_64I          => Some fct3_101
  | SRAIW_64I          => Some fct3_101
  | ADDW_64I           => Some fct3_000
  | SUBW_64I           => Some fct3_000
  | SLLW_64I           => Some fct3_001
  | SRLW_64I           => Some fct3_101
  | SRAW_64I           => Some fct3_101
  | FENCE_I_32Zifencei => Some fct3_001
  | FENCE_I_64Zifencei => Some fct3_001
  | CSRRW_32Zicsr      => Some fct3_001
  | CSRRS_32Zicsr      => Some fct3_010
  | CSRRC_32Zicsr      => Some fct3_011
  | CSRRWI_32Zicsr     => Some fct3_101
  | CSRRSI_32Zicsr     => Some fct3_110
  | CSRRCI_32Zicsr     => Some fct3_111
  | CSRRW_64Zicsr      => Some fct3_001
  | CSRRS_64Zicsr      => Some fct3_010
  | CSRRC_64Zicsr      => Some fct3_011
  | CSRRWI_64Zicsr     => Some fct3_101
  | CSRRSI_64Zicsr     => Some fct3_110
  | CSRRCI_64Zicsr     => Some fct3_111
  | MUL_32M            => Some fct3_000
  | MULH_32M           => Some fct3_001
  | MULHSU_32M         => Some fct3_010
  | MULHU_32M          => Some fct3_011
  | DIV_32M            => Some fct3_100
  | DIVU_32M           => Some fct3_101
  | REM_32M            => Some fct3_110
  | REMU_32M           => Some fct3_111
  | MUL_64M            => Some fct3_000
  | MULH_64M           => Some fct3_001
  | MULHSU_64M         => Some fct3_010
  | MULHU_64M          => Some fct3_011
  | DIV_64M            => Some fct3_100
  | DIVU_64M           => Some fct3_101
  | REM_64M            => Some fct3_110
  | REMU_64M           => Some fct3_111
  | MULW_64M           => Some fct3_000
  | DIVW_64M           => Some fct3_100
  | DIVUW_64M          => Some fct3_101
  | REMW_64M           => Some fct3_110
  | REMUW_64M          => Some fct3_111
  | LR_W_00_32A        => Some fct3_010
  | LR_W_01_32A        => Some fct3_010
  | LR_W_10_32A        => Some fct3_010
  | LR_W_11_32A        => Some fct3_010
  | SC_W_00_32A        => Some fct3_010
  | SC_W_01_32A        => Some fct3_010
  | SC_W_10_32A        => Some fct3_010
  | SC_W_11_32A        => Some fct3_010
  | AMOSWAP_W_00_32A   => Some fct3_010
  | AMOSWAP_W_01_32A   => Some fct3_010
  | AMOSWAP_W_10_32A   => Some fct3_010
  | AMOSWAP_W_11_32A   => Some fct3_010
  | AMOADD_W_00_32A    => Some fct3_010
  | AMOADD_W_01_32A    => Some fct3_010
  | AMOADD_W_10_32A    => Some fct3_010
  | AMOADD_W_11_32A    => Some fct3_010
  | AMOXOR_W_00_32A    => Some fct3_010
  | AMOXOR_W_01_32A    => Some fct3_010
  | AMOXOR_W_10_32A    => Some fct3_010
  | AMOXOR_W_11_32A    => Some fct3_010
  | AMOAND_W_00_32A    => Some fct3_010
  | AMOAND_W_01_32A    => Some fct3_010
  | AMOAND_W_10_32A    => Some fct3_010
  | AMOAND_W_11_32A    => Some fct3_010
  | AMOOR_W_00_32A     => Some fct3_010
  | AMOOR_W_01_32A     => Some fct3_010
  | AMOOR_W_10_32A     => Some fct3_010
  | AMOOR_W_11_32A     => Some fct3_010
  | AMOMIN_W_00_32A    => Some fct3_010
  | AMOMIN_W_01_32A    => Some fct3_010
  | AMOMIN_W_10_32A    => Some fct3_010
  | AMOMIN_W_11_32A    => Some fct3_010
  | AMOMAX_W_00_32A    => Some fct3_010
  | AMOMAX_W_01_32A    => Some fct3_010
  | AMOMAX_W_10_32A    => Some fct3_010
  | AMOMAX_W_11_32A    => Some fct3_010
  | AMOMINU_W_00_32A   => Some fct3_010
  | AMOMINU_W_01_32A   => Some fct3_010
  | AMOMINU_W_10_32A   => Some fct3_010
  | AMOMINU_W_11_32A   => Some fct3_010
  | AMOMAXU_W_00_32A   => Some fct3_010
  | AMOMAXU_W_01_32A   => Some fct3_010
  | AMOMAXU_W_10_32A   => Some fct3_010
  | AMOMAXU_W_11_32A   => Some fct3_010
  | LR_W_00_64A        => Some fct3_010
  | LR_W_01_64A        => Some fct3_010
  | LR_W_10_64A        => Some fct3_010
  | LR_W_11_64A        => Some fct3_010
  | SC_W_00_64A        => Some fct3_010
  | SC_W_01_64A        => Some fct3_010
  | SC_W_10_64A        => Some fct3_010
  | SC_W_11_64A        => Some fct3_010
  | AMOSWAP_W_00_64A   => Some fct3_010
  | AMOSWAP_W_01_64A   => Some fct3_010
  | AMOSWAP_W_10_64A   => Some fct3_010
  | AMOSWAP_W_11_64A   => Some fct3_010
  | AMOADD_W_00_64A    => Some fct3_010
  | AMOADD_W_01_64A    => Some fct3_010
  | AMOADD_W_10_64A    => Some fct3_010
  | AMOADD_W_11_64A    => Some fct3_010
  | AMOXOR_W_00_64A    => Some fct3_010
  | AMOXOR_W_01_64A    => Some fct3_010
  | AMOXOR_W_10_64A    => Some fct3_010
  | AMOXOR_W_11_64A    => Some fct3_010
  | AMOAND_W_00_64A    => Some fct3_010
  | AMOAND_W_01_64A    => Some fct3_010
  | AMOAND_W_10_64A    => Some fct3_010
  | AMOAND_W_11_64A    => Some fct3_010
  | AMOOR_W_00_64A     => Some fct3_010
  | AMOOR_W_01_64A     => Some fct3_010
  | AMOOR_W_10_64A     => Some fct3_010
  | AMOOR_W_11_64A     => Some fct3_010
  | AMOMIN_W_00_64A    => Some fct3_010
  | AMOMIN_W_01_64A    => Some fct3_010
  | AMOMIN_W_10_64A    => Some fct3_010
  | AMOMIN_W_11_64A    => Some fct3_010
  | AMOMAX_W_00_64A    => Some fct3_010
  | AMOMAX_W_01_64A    => Some fct3_010
  | AMOMAX_W_10_64A    => Some fct3_010
  | AMOMAX_W_11_64A    => Some fct3_010
  | AMOMINU_W_00_64A   => Some fct3_010
  | AMOMINU_W_01_64A   => Some fct3_010
  | AMOMINU_W_10_64A   => Some fct3_010
  | AMOMINU_W_11_64A   => Some fct3_010
  | AMOMAXU_W_00_64A   => Some fct3_010
  | AMOMAXU_W_01_64A   => Some fct3_010
  | AMOMAXU_W_10_64A   => Some fct3_010
  | AMOMAXU_W_11_64A   => Some fct3_010
  | LR_D_00_64A        => Some fct3_011
  | LR_D_01_64A        => Some fct3_011
  | LR_D_10_64A        => Some fct3_011
  | LR_D_11_64A        => Some fct3_011
  | SC_D_00_64A        => Some fct3_011
  | SC_D_01_64A        => Some fct3_011
  | SC_D_10_64A        => Some fct3_011
  | SC_D_11_64A        => Some fct3_011
  | AMOSWAP_D_00_64A   => Some fct3_011
  | AMOSWAP_D_01_64A   => Some fct3_011
  | AMOSWAP_D_10_64A   => Some fct3_011
  | AMOSWAP_D_11_64A   => Some fct3_011
  | AMOADD_D_00_64A    => Some fct3_011
  | AMOADD_D_01_64A    => Some fct3_011
  | AMOADD_D_10_64A    => Some fct3_011
  | AMOADD_D_11_64A    => Some fct3_011
  | AMOXOR_D_00_64A    => Some fct3_011
  | AMOXOR_D_01_64A    => Some fct3_011
  | AMOXOR_D_10_64A    => Some fct3_011
  | AMOXOR_D_11_64A    => Some fct3_011
  | AMOAND_D_00_64A    => Some fct3_011
  | AMOAND_D_01_64A    => Some fct3_011
  | AMOAND_D_10_64A    => Some fct3_011
  | AMOAND_D_11_64A    => Some fct3_011
  | AMOOR_D_00_64A     => Some fct3_011
  | AMOOR_D_01_64A     => Some fct3_011
  | AMOOR_D_10_64A     => Some fct3_011
  | AMOOR_D_11_64A     => Some fct3_011
  | AMOMIN_D_00_64A    => Some fct3_011
  | AMOMIN_D_01_64A    => Some fct3_011
  | AMOMIN_D_10_64A    => Some fct3_011
  | AMOMIN_D_11_64A    => Some fct3_011
  | AMOMAX_D_00_64A    => Some fct3_011
  | AMOMAX_D_01_64A    => Some fct3_011
  | AMOMAX_D_10_64A    => Some fct3_011
  | AMOMAX_D_11_64A    => Some fct3_011
  | AMOMINU_D_00_64A   => Some fct3_011
  | AMOMINU_D_01_64A   => Some fct3_011
  | AMOMINU_D_10_64A   => Some fct3_011
  | AMOMINU_D_11_64A   => Some fct3_011
  | AMOMAXU_D_00_64A   => Some fct3_011
  | AMOMAXU_D_01_64A   => Some fct3_011
  | AMOMAXU_D_10_64A   => Some fct3_011
  | AMOMAXU_D_11_64A   => Some fct3_011
  | FLW_32F            => Some fct3_010
  | FSW_32F            => Some fct3_010
  | FMADD_RNE_S_32F    => Some fct3_000
  | FMADD_RTZ_S_32F    => Some fct3_001
  | FMADD_RDN_S_32F    => Some fct3_010
  | FMADD_RUP_S_32F    => Some fct3_011
  | FMADD_RMM_S_32F    => Some fct3_100
  | FMADD_DYN_S_32F    => Some fct3_111
  | FMSUB_RNE_S_32F    => Some fct3_000
  | FMSUB_RTZ_S_32F    => Some fct3_001
  | FMSUB_RDN_S_32F    => Some fct3_010
  | FMSUB_RUP_S_32F    => Some fct3_011
  | FMSUB_RMM_S_32F    => Some fct3_100
  | FMSUB_DYN_S_32F    => Some fct3_111
  | FNMSUB_RNE_S_32F   => Some fct3_000
  | FNMSUB_RTZ_S_32F   => Some fct3_001
  | FNMSUB_RDN_S_32F   => Some fct3_010
  | FNMSUB_RUP_S_32F   => Some fct3_011
  | FNMSUB_RMM_S_32F   => Some fct3_100
  | FNMSUB_DYN_S_32F   => Some fct3_111
  | FNMADD_RNE_S_32F   => Some fct3_000
  | FNMADD_RTZ_S_32F   => Some fct3_001
  | FNMADD_RDN_S_32F   => Some fct3_010
  | FNMADD_RUP_S_32F   => Some fct3_011
  | FNMADD_RMM_S_32F   => Some fct3_100
  | FNMADD_DYN_S_32F   => Some fct3_111
  | FADD_RNE_S_32F     => Some fct3_000
  | FADD_RTZ_S_32F     => Some fct3_001
  | FADD_RDN_S_32F     => Some fct3_010
  | FADD_RUP_S_32F     => Some fct3_011
  | FADD_RMM_S_32F     => Some fct3_100
  | FADD_DYN_S_32F     => Some fct3_111
  | FSUB_RNE_S_32F     => Some fct3_000
  | FSUB_RTZ_S_32F     => Some fct3_001
  | FSUB_RDN_S_32F     => Some fct3_010
  | FSUB_RUP_S_32F     => Some fct3_011
  | FSUB_RMM_S_32F     => Some fct3_100
  | FSUB_DYN_S_32F     => Some fct3_111
  | FMUL_RNE_S_32F     => Some fct3_000
  | FMUL_RTZ_S_32F     => Some fct3_001
  | FMUL_RDN_S_32F     => Some fct3_010
  | FMUL_RUP_S_32F     => Some fct3_011
  | FMUL_RMM_S_32F     => Some fct3_100
  | FMUL_DYN_S_32F     => Some fct3_111
  | FDIV_RNE_S_32F     => Some fct3_000
  | FDIV_RTZ_S_32F     => Some fct3_001
  | FDIV_RDN_S_32F     => Some fct3_010
  | FDIV_RUP_S_32F     => Some fct3_011
  | FDIV_RMM_S_32F     => Some fct3_100
  | FDIV_DYN_S_32F     => Some fct3_111
  | FSQRT_RNE_S_32F    => Some fct3_000
  | FSQRT_RTZ_S_32F    => Some fct3_001
  | FSQRT_RDN_S_32F    => Some fct3_010
  | FSQRT_RUP_S_32F    => Some fct3_011
  | FSQRT_RMM_S_32F    => Some fct3_100
  | FSQRT_DYN_S_32F    => Some fct3_111
  | FSGNJ_S_32F        => Some fct3_000
  | FSGNJN_S_32F       => Some fct3_001
  | FSGNJX_S_32F       => Some fct3_010
  | FMIN_S_32F         => Some fct3_000
  | FMAX_S_32F         => Some fct3_001
  | FCVT_RNE_W_S_32F   => Some fct3_000
  | FCVT_RTZ_W_S_32F   => Some fct3_001
  | FCVT_RDN_W_S_32F   => Some fct3_010
  | FCVT_RUP_W_S_32F   => Some fct3_011
  | FCVT_RMM_W_S_32F   => Some fct3_100
  | FCVT_DYN_W_S_32F   => Some fct3_111
  | FCVT_RNE_WU_S_32F  => Some fct3_000
  | FCVT_RTZ_WU_S_32F  => Some fct3_001
  | FCVT_RDN_WU_S_32F  => Some fct3_010
  | FCVT_RUP_WU_S_32F  => Some fct3_011
  | FCVT_RMM_WU_S_32F  => Some fct3_100
  | FCVT_DYN_WU_S_32F  => Some fct3_111
  | FMV_X_W_32F        => Some fct3_000
  | FEQ_S_32F          => Some fct3_010
  | FLT_S_32F          => Some fct3_001
  | FLE_S_32F          => Some fct3_000
  | FCLASS_S_32F       => Some fct3_001
  | FCVT_RNE_S_W_32F   => Some fct3_000
  | FCVT_RTZ_S_W_32F   => Some fct3_001
  | FCVT_RDN_S_W_32F   => Some fct3_010
  | FCVT_RUP_S_W_32F   => Some fct3_011
  | FCVT_RMM_S_W_32F   => Some fct3_100
  | FCVT_DYN_S_W_32F   => Some fct3_111
  | FCVT_RNE_S_WU_32F  => Some fct3_000
  | FCVT_RTZ_S_WU_32F  => Some fct3_001
  | FCVT_RDN_S_WU_32F  => Some fct3_010
  | FCVT_RUP_S_WU_32F  => Some fct3_011
  | FCVT_RMM_S_WU_32F  => Some fct3_100
  | FCVT_DYN_S_WU_32F  => Some fct3_111
  | FMV_W_X_32F        => Some fct3_000
  | FLW_64F            => Some fct3_010
  | FSW_64F            => Some fct3_010
  | FMADD_RNE_S_64F    => Some fct3_000
  | FMADD_RTZ_S_64F    => Some fct3_001
  | FMADD_RDN_S_64F    => Some fct3_010
  | FMADD_RUP_S_64F    => Some fct3_011
  | FMADD_RMM_S_64F    => Some fct3_100
  | FMADD_DYN_S_64F    => Some fct3_111
  | FMSUB_RNE_S_64F    => Some fct3_000
  | FMSUB_RTZ_S_64F    => Some fct3_001
  | FMSUB_RDN_S_64F    => Some fct3_010
  | FMSUB_RUP_S_64F    => Some fct3_011
  | FMSUB_RMM_S_64F    => Some fct3_100
  | FMSUB_DYN_S_64F    => Some fct3_111
  | FNMSUB_RNE_S_64F   => Some fct3_000
  | FNMSUB_RTZ_S_64F   => Some fct3_001
  | FNMSUB_RDN_S_64F   => Some fct3_010
  | FNMSUB_RUP_S_64F   => Some fct3_011
  | FNMSUB_RMM_S_64F   => Some fct3_100
  | FNMSUB_DYN_S_64F   => Some fct3_111
  | FNMADD_RNE_S_64F   => Some fct3_000
  | FNMADD_RTZ_S_64F   => Some fct3_001
  | FNMADD_RDN_S_64F   => Some fct3_010
  | FNMADD_RUP_S_64F   => Some fct3_011
  | FNMADD_RMM_S_64F   => Some fct3_100
  | FNMADD_DYN_S_64F   => Some fct3_111
  | FADD_RNE_S_64F     => Some fct3_000
  | FADD_RTZ_S_64F     => Some fct3_001
  | FADD_RDN_S_64F     => Some fct3_010
  | FADD_RUP_S_64F     => Some fct3_011
  | FADD_RMM_S_64F     => Some fct3_100
  | FADD_DYN_S_64F     => Some fct3_111
  | FSUB_RNE_S_64F     => Some fct3_000
  | FSUB_RTZ_S_64F     => Some fct3_001
  | FSUB_RDN_S_64F     => Some fct3_010
  | FSUB_RUP_S_64F     => Some fct3_011
  | FSUB_RMM_S_64F     => Some fct3_100
  | FSUB_DYN_S_64F     => Some fct3_111
  | FMUL_RNE_S_64F     => Some fct3_000
  | FMUL_RTZ_S_64F     => Some fct3_001
  | FMUL_RDN_S_64F     => Some fct3_010
  | FMUL_RUP_S_64F     => Some fct3_011
  | FMUL_RMM_S_64F     => Some fct3_100
  | FMUL_DYN_S_64F     => Some fct3_111
  | FDIV_RNE_S_64F     => Some fct3_000
  | FDIV_RTZ_S_64F     => Some fct3_001
  | FDIV_RDN_S_64F     => Some fct3_010
  | FDIV_RUP_S_64F     => Some fct3_011
  | FDIV_RMM_S_64F     => Some fct3_100
  | FDIV_DYN_S_64F     => Some fct3_111
  | FSQRT_RNE_S_64F    => Some fct3_000
  | FSQRT_RTZ_S_64F    => Some fct3_001
  | FSQRT_RDN_S_64F    => Some fct3_010
  | FSQRT_RUP_S_64F    => Some fct3_011
  | FSQRT_RMM_S_64F    => Some fct3_100
  | FSQRT_DYN_S_64F    => Some fct3_111
  | FSGNJ_S_64F        => Some fct3_000
  | FSGNJN_S_64F       => Some fct3_001
  | FSGNJX_S_64F       => Some fct3_010
  | FMIN_S_64F         => Some fct3_000
  | FMAX_S_64F         => Some fct3_001
  | FCVT_RNE_W_S_64F   => Some fct3_000
  | FCVT_RTZ_W_S_64F   => Some fct3_001
  | FCVT_RDN_W_S_64F   => Some fct3_010
  | FCVT_RUP_W_S_64F   => Some fct3_011
  | FCVT_RMM_W_S_64F   => Some fct3_100
  | FCVT_DYN_W_S_64F   => Some fct3_111
  | FCVT_RNE_WU_S_64F  => Some fct3_000
  | FCVT_RTZ_WU_S_64F  => Some fct3_001
  | FCVT_RDN_WU_S_64F  => Some fct3_010
  | FCVT_RUP_WU_S_64F  => Some fct3_011
  | FCVT_RMM_WU_S_64F  => Some fct3_100
  | FCVT_DYN_WU_S_64F  => Some fct3_111
  | FMV_X_W_64F        => Some fct3_000
  | FEQ_S_64F          => Some fct3_010
  | FLT_S_64F          => Some fct3_001
  | FLE_S_64F          => Some fct3_000
  | FCLASS_S_64F       => Some fct3_001
  | FCVT_RNE_S_W_64F   => Some fct3_000
  | FCVT_RTZ_S_W_64F   => Some fct3_001
  | FCVT_RDN_S_W_64F   => Some fct3_010
  | FCVT_RUP_S_W_64F   => Some fct3_011
  | FCVT_RMM_S_W_64F   => Some fct3_100
  | FCVT_DYN_S_W_64F   => Some fct3_111
  | FCVT_RNE_S_WU_64F  => Some fct3_000
  | FCVT_RTZ_S_WU_64F  => Some fct3_001
  | FCVT_RDN_S_WU_64F  => Some fct3_010
  | FCVT_RUP_S_WU_64F  => Some fct3_011
  | FCVT_RMM_S_WU_64F  => Some fct3_100
  | FCVT_DYN_S_WU_64F  => Some fct3_111
  | FMV_W_X_64F        => Some fct3_000
  | FCVT_RNE_L_S_64F   => Some fct3_000
  | FCVT_RTZ_L_S_64F   => Some fct3_001
  | FCVT_RDN_L_S_64F   => Some fct3_010
  | FCVT_RUP_L_S_64F   => Some fct3_011
  | FCVT_RMM_L_S_64F   => Some fct3_100
  | FCVT_DYN_L_S_64F   => Some fct3_111
  | FCVT_RNE_LU_S_64F  => Some fct3_000
  | FCVT_RTZ_LU_S_64F  => Some fct3_001
  | FCVT_RDN_LU_S_64F  => Some fct3_010
  | FCVT_RUP_LU_S_64F  => Some fct3_011
  | FCVT_RMM_LU_S_64F  => Some fct3_100
  | FCVT_DYN_LU_S_64F  => Some fct3_111
  | FCVT_RNE_S_L_64F   => Some fct3_000
  | FCVT_RTZ_S_L_64F   => Some fct3_001
  | FCVT_RDN_S_L_64F   => Some fct3_010
  | FCVT_RUP_S_L_64F   => Some fct3_011
  | FCVT_RMM_S_L_64F   => Some fct3_100
  | FCVT_DYN_S_L_64F   => Some fct3_111
  | FCVT_RNE_S_LU_64F  => Some fct3_000
  | FCVT_RTZ_S_LU_64F  => Some fct3_001
  | FCVT_RDN_S_LU_64F  => Some fct3_010
  | FCVT_RUP_S_LU_64F  => Some fct3_011
  | FCVT_RMM_S_LU_64F  => Some fct3_100
  | FCVT_DYN_S_LU_64F  => Some fct3_111
  | FLD_32D            => Some fct3_011
  | FSD_32D            => Some fct3_011
  | FMADD_RNE_D_32D    => Some fct3_000
  | FMADD_RTZ_D_32D    => Some fct3_001
  | FMADD_RDN_D_32D    => Some fct3_010
  | FMADD_RUP_D_32D    => Some fct3_011
  | FMADD_RMM_D_32D    => Some fct3_100
  | FMADD_DYN_D_32D    => Some fct3_111
  | FMSUB_RNE_D_32D    => Some fct3_000
  | FMSUB_RTZ_D_32D    => Some fct3_001
  | FMSUB_RDN_D_32D    => Some fct3_010
  | FMSUB_RUP_D_32D    => Some fct3_011
  | FMSUB_RMM_D_32D    => Some fct3_100
  | FMSUB_DYN_D_32D    => Some fct3_111
  | FNMSUB_RNE_D_32D   => Some fct3_000
  | FNMSUB_RTZ_D_32D   => Some fct3_001
  | FNMSUB_RDN_D_32D   => Some fct3_010
  | FNMSUB_RUP_D_32D   => Some fct3_011
  | FNMSUB_RMM_D_32D   => Some fct3_100
  | FNMSUB_DYN_D_32D   => Some fct3_111
  | FNMADD_RNE_D_32D   => Some fct3_000
  | FNMADD_RTZ_D_32D   => Some fct3_001
  | FNMADD_RDN_D_32D   => Some fct3_010
  | FNMADD_RUP_D_32D   => Some fct3_011
  | FNMADD_RMM_D_32D   => Some fct3_100
  | FNMADD_DYN_D_32D   => Some fct3_111
  | FADD_RNE_D_32D     => Some fct3_000
  | FADD_RTZ_D_32D     => Some fct3_001
  | FADD_RDN_D_32D     => Some fct3_010
  | FADD_RUP_D_32D     => Some fct3_011
  | FADD_RMM_D_32D     => Some fct3_100
  | FADD_DYN_D_32D     => Some fct3_111
  | FSUB_RNE_D_32D     => Some fct3_000
  | FSUB_RTZ_D_32D     => Some fct3_001
  | FSUB_RDN_D_32D     => Some fct3_010
  | FSUB_RUP_D_32D     => Some fct3_011
  | FSUB_RMM_D_32D     => Some fct3_100
  | FSUB_DYN_D_32D     => Some fct3_111
  | FMUL_RNE_D_32D     => Some fct3_000
  | FMUL_RTZ_D_32D     => Some fct3_001
  | FMUL_RDN_D_32D     => Some fct3_010
  | FMUL_RUP_D_32D     => Some fct3_011
  | FMUL_RMM_D_32D     => Some fct3_100
  | FMUL_DYN_D_32D     => Some fct3_111
  | FDIV_RNE_D_32D     => Some fct3_000
  | FDIV_RTZ_D_32D     => Some fct3_001
  | FDIV_RDN_D_32D     => Some fct3_010
  | FDIV_RUP_D_32D     => Some fct3_011
  | FDIV_RMM_D_32D     => Some fct3_100
  | FDIV_DYN_D_32D     => Some fct3_111
  | FSQRT_RNE_D_32D    => Some fct3_000
  | FSQRT_RTZ_D_32D    => Some fct3_001
  | FSQRT_RDN_D_32D    => Some fct3_010
  | FSQRT_RUP_D_32D    => Some fct3_011
  | FSQRT_RMM_D_32D    => Some fct3_100
  | FSQRT_DYN_D_32D    => Some fct3_111
  | FSGNJ_D_32D        => Some fct3_000
  | FSGNJN_D_32D       => Some fct3_001
  | FSGNJX_D_32D       => Some fct3_010
  | FMIN_D_32D         => Some fct3_000
  | FMAX_D_32D         => Some fct3_001
  | FCVT_RNE_S_D_32D   => Some fct3_000
  | FCVT_RTZ_S_D_32D   => Some fct3_001
  | FCVT_RDN_S_D_32D   => Some fct3_010
  | FCVT_RUP_S_D_32D   => Some fct3_011
  | FCVT_RMM_S_D_32D   => Some fct3_100
  | FCVT_DYN_S_D_32D   => Some fct3_111
  | FCVT_RNE_D_S_32D   => Some fct3_000
  | FCVT_RTZ_D_S_32D   => Some fct3_001
  | FCVT_RDN_D_S_32D   => Some fct3_010
  | FCVT_RUP_D_S_32D   => Some fct3_011
  | FCVT_RMM_D_S_32D   => Some fct3_100
  | FCVT_DYN_D_S_32D   => Some fct3_111
  | FEQ_D_32D          => Some fct3_010
  | FLT_D_32D          => Some fct3_001
  | FLE_D_32D          => Some fct3_000
  | FCLASS_D_32D       => Some fct3_001
  | FCVT_RNE_W_D_32D   => Some fct3_000
  | FCVT_RTZ_W_D_32D   => Some fct3_001
  | FCVT_RDN_W_D_32D   => Some fct3_010
  | FCVT_RUP_W_D_32D   => Some fct3_011
  | FCVT_RMM_W_D_32D   => Some fct3_100
  | FCVT_DYN_W_D_32D   => Some fct3_111
  | FCVT_RNE_WU_D_32D  => Some fct3_000
  | FCVT_RTZ_WU_D_32D  => Some fct3_001
  | FCVT_RDN_WU_D_32D  => Some fct3_010
  | FCVT_RUP_WU_D_32D  => Some fct3_011
  | FCVT_RMM_WU_D_32D  => Some fct3_100
  | FCVT_DYN_WU_D_32D  => Some fct3_111
  | FCVT_RNE_D_W_32D   => Some fct3_000
  | FCVT_RTZ_D_W_32D   => Some fct3_001
  | FCVT_RDN_D_W_32D   => Some fct3_010
  | FCVT_RUP_D_W_32D   => Some fct3_011
  | FCVT_RMM_D_W_32D   => Some fct3_100
  | FCVT_DYN_D_W_32D   => Some fct3_111
  | FCVT_RNE_D_WU_32D  => Some fct3_000
  | FCVT_RTZ_D_WU_32D  => Some fct3_001
  | FCVT_RDN_D_WU_32D  => Some fct3_010
  | FCVT_RUP_D_WU_32D  => Some fct3_011
  | FCVT_RMM_D_WU_32D  => Some fct3_100
  | FCVT_DYN_D_WU_32D  => Some fct3_111
  | FLD_64D            => Some fct3_011
  | FSD_64D            => Some fct3_011
  | FMADD_RNE_D_64D    => Some fct3_000
  | FMADD_RTZ_D_64D    => Some fct3_001
  | FMADD_RDN_D_64D    => Some fct3_010
  | FMADD_RUP_D_64D    => Some fct3_011
  | FMADD_RMM_D_64D    => Some fct3_100
  | FMADD_DYN_D_64D    => Some fct3_111
  | FMSUB_RNE_D_64D    => Some fct3_000
  | FMSUB_RTZ_D_64D    => Some fct3_001
  | FMSUB_RDN_D_64D    => Some fct3_010
  | FMSUB_RUP_D_64D    => Some fct3_011
  | FMSUB_RMM_D_64D    => Some fct3_100
  | FMSUB_DYN_D_64D    => Some fct3_111
  | FNMSUB_RNE_D_64D   => Some fct3_000
  | FNMSUB_RTZ_D_64D   => Some fct3_001
  | FNMSUB_RDN_D_64D   => Some fct3_010
  | FNMSUB_RUP_D_64D   => Some fct3_011
  | FNMSUB_RMM_D_64D   => Some fct3_100
  | FNMSUB_DYN_D_64D   => Some fct3_111
  | FNMADD_RNE_D_64D   => Some fct3_000
  | FNMADD_RTZ_D_64D   => Some fct3_001
  | FNMADD_RDN_D_64D   => Some fct3_010
  | FNMADD_RUP_D_64D   => Some fct3_011
  | FNMADD_RMM_D_64D   => Some fct3_100
  | FNMADD_DYN_D_64D   => Some fct3_111
  | FADD_RNE_D_64D     => Some fct3_000
  | FADD_RTZ_D_64D     => Some fct3_001
  | FADD_RDN_D_64D     => Some fct3_010
  | FADD_RUP_D_64D     => Some fct3_011
  | FADD_RMM_D_64D     => Some fct3_100
  | FADD_DYN_D_64D     => Some fct3_111
  | FSUB_RNE_D_64D     => Some fct3_000
  | FSUB_RTZ_D_64D     => Some fct3_001
  | FSUB_RDN_D_64D     => Some fct3_010
  | FSUB_RUP_D_64D     => Some fct3_011
  | FSUB_RMM_D_64D     => Some fct3_100
  | FSUB_DYN_D_64D     => Some fct3_111
  | FMUL_RNE_D_64D     => Some fct3_000
  | FMUL_RTZ_D_64D     => Some fct3_001
  | FMUL_RDN_D_64D     => Some fct3_010
  | FMUL_RUP_D_64D     => Some fct3_011
  | FMUL_RMM_D_64D     => Some fct3_100
  | FMUL_DYN_D_64D     => Some fct3_111
  | FDIV_RNE_D_64D     => Some fct3_000
  | FDIV_RTZ_D_64D     => Some fct3_001
  | FDIV_RDN_D_64D     => Some fct3_010
  | FDIV_RUP_D_64D     => Some fct3_011
  | FDIV_RMM_D_64D     => Some fct3_100
  | FDIV_DYN_D_64D     => Some fct3_111
  | FSQRT_RNE_D_64D    => Some fct3_000
  | FSQRT_RTZ_D_64D    => Some fct3_001
  | FSQRT_RDN_D_64D    => Some fct3_010
  | FSQRT_RUP_D_64D    => Some fct3_011
  | FSQRT_RMM_D_64D    => Some fct3_100
  | FSQRT_DYN_D_64D    => Some fct3_111
  | FSGNJ_D_64D        => Some fct3_000
  | FSGNJN_D_64D       => Some fct3_001
  | FSGNJX_D_64D       => Some fct3_010
  | FMIN_D_64D         => Some fct3_000
  | FMAX_D_64D         => Some fct3_001
  | FCVT_RNE_S_D_64D   => Some fct3_000
  | FCVT_RTZ_S_D_64D   => Some fct3_001
  | FCVT_RDN_S_D_64D   => Some fct3_010
  | FCVT_RUP_S_D_64D   => Some fct3_011
  | FCVT_RMM_S_D_64D   => Some fct3_100
  | FCVT_DYN_S_D_64D   => Some fct3_111
  | FCVT_RNE_D_S_64D   => Some fct3_000
  | FCVT_RTZ_D_S_64D   => Some fct3_001
  | FCVT_RDN_D_S_64D   => Some fct3_010
  | FCVT_RUP_D_S_64D   => Some fct3_011
  | FCVT_RMM_D_S_64D   => Some fct3_100
  | FCVT_DYN_D_S_64D   => Some fct3_111
  | FEQ_D_64D          => Some fct3_010
  | FLT_D_64D          => Some fct3_001
  | FLE_D_64D          => Some fct3_000
  | FCLASS_D_64D       => Some fct3_001
  | FCVT_RNE_W_D_64D   => Some fct3_000
  | FCVT_RTZ_W_D_64D   => Some fct3_001
  | FCVT_RDN_W_D_64D   => Some fct3_010
  | FCVT_RUP_W_D_64D   => Some fct3_011
  | FCVT_RMM_W_D_64D   => Some fct3_100
  | FCVT_DYN_W_D_64D   => Some fct3_111
  | FCVT_RNE_WU_D_64D  => Some fct3_000
  | FCVT_RTZ_WU_D_64D  => Some fct3_001
  | FCVT_RDN_WU_D_64D  => Some fct3_010
  | FCVT_RUP_WU_D_64D  => Some fct3_011
  | FCVT_RMM_WU_D_64D  => Some fct3_100
  | FCVT_DYN_WU_D_64D  => Some fct3_111
  | FCVT_RNE_D_W_64D   => Some fct3_000
  | FCVT_RTZ_D_W_64D   => Some fct3_001
  | FCVT_RDN_D_W_64D   => Some fct3_010
  | FCVT_RUP_D_W_64D   => Some fct3_011
  | FCVT_RMM_D_W_64D   => Some fct3_100
  | FCVT_DYN_D_W_64D   => Some fct3_111
  | FCVT_RNE_D_WU_64D  => Some fct3_000
  | FCVT_RTZ_D_WU_64D  => Some fct3_001
  | FCVT_RDN_D_WU_64D  => Some fct3_010
  | FCVT_RUP_D_WU_64D  => Some fct3_011
  | FCVT_RMM_D_WU_64D  => Some fct3_100
  | FCVT_DYN_D_WU_64D  => Some fct3_111
  | FCVT_RNE_L_D_64D   => Some fct3_000
  | FCVT_RTZ_L_D_64D   => Some fct3_001
  | FCVT_RDN_L_D_64D   => Some fct3_010
  | FCVT_RUP_L_D_64D   => Some fct3_011
  | FCVT_RMM_L_D_64D   => Some fct3_100
  | FCVT_DYN_L_D_64D   => Some fct3_111
  | FCVT_RNE_LU_D_64D  => Some fct3_000
  | FCVT_RTZ_LU_D_64D  => Some fct3_001
  | FCVT_RDN_LU_D_64D  => Some fct3_010
  | FCVT_RUP_LU_D_64D  => Some fct3_011
  | FCVT_RMM_LU_D_64D  => Some fct3_100
  | FCVT_DYN_LU_D_64D  => Some fct3_111
  | FMV_X_D_64D        => Some fct3_000
  | FCVT_RNE_D_L_64D   => Some fct3_000
  | FCVT_RTZ_D_L_64D   => Some fct3_001
  | FCVT_RDN_D_L_64D   => Some fct3_010
  | FCVT_RUP_D_L_64D   => Some fct3_011
  | FCVT_RMM_D_L_64D   => Some fct3_100
  | FCVT_DYN_D_L_64D   => Some fct3_111
  | FCVT_RNE_D_LU_64D  => Some fct3_000
  | FCVT_RTZ_D_LU_64D  => Some fct3_001
  | FCVT_RDN_D_LU_64D  => Some fct3_010
  | FCVT_RUP_D_LU_64D  => Some fct3_011
  | FCVT_RMM_D_LU_64D  => Some fct3_100
  | FCVT_DYN_D_LU_64D  => Some fct3_111
  | FMV_D_X_64D        => Some fct3_000
  | FLQ_32Q            => Some fct3_100
  | FSQ_32Q            => Some fct3_100
  | FMADD_RNE_Q_32Q    => Some fct3_000
  | FMADD_RTZ_Q_32Q    => Some fct3_001
  | FMADD_RDN_Q_32Q    => Some fct3_010
  | FMADD_RUP_Q_32Q    => Some fct3_011
  | FMADD_RMM_Q_32Q    => Some fct3_100
  | FMADD_DYN_Q_32Q    => Some fct3_111
  | FMSUB_RNE_Q_32Q    => Some fct3_000
  | FMSUB_RTZ_Q_32Q    => Some fct3_001
  | FMSUB_RDN_Q_32Q    => Some fct3_010
  | FMSUB_RUP_Q_32Q    => Some fct3_011
  | FMSUB_RMM_Q_32Q    => Some fct3_100
  | FMSUB_DYN_Q_32Q    => Some fct3_111
  | FNMSUB_RNE_Q_32Q   => Some fct3_000
  | FNMSUB_RTZ_Q_32Q   => Some fct3_001
  | FNMSUB_RDN_Q_32Q   => Some fct3_010
  | FNMSUB_RUP_Q_32Q   => Some fct3_011
  | FNMSUB_RMM_Q_32Q   => Some fct3_100
  | FNMSUB_DYN_Q_32Q   => Some fct3_111
  | FNMADD_RNE_Q_32Q   => Some fct3_000
  | FNMADD_RTZ_Q_32Q   => Some fct3_001
  | FNMADD_RDN_Q_32Q   => Some fct3_010
  | FNMADD_RUP_Q_32Q   => Some fct3_011
  | FNMADD_RMM_Q_32Q   => Some fct3_100
  | FNMADD_DYN_Q_32Q   => Some fct3_111
  | FADD_RNE_Q_32Q     => Some fct3_000
  | FADD_RTZ_Q_32Q     => Some fct3_001
  | FADD_RDN_Q_32Q     => Some fct3_010
  | FADD_RUP_Q_32Q     => Some fct3_011
  | FADD_RMM_Q_32Q     => Some fct3_100
  | FADD_DYN_Q_32Q     => Some fct3_111
  | FSUB_RNE_Q_32Q     => Some fct3_000
  | FSUB_RTZ_Q_32Q     => Some fct3_001
  | FSUB_RDN_Q_32Q     => Some fct3_010
  | FSUB_RUP_Q_32Q     => Some fct3_011
  | FSUB_RMM_Q_32Q     => Some fct3_100
  | FSUB_DYN_Q_32Q     => Some fct3_111
  | FMUL_RNE_Q_32Q     => Some fct3_000
  | FMUL_RTZ_Q_32Q     => Some fct3_001
  | FMUL_RDN_Q_32Q     => Some fct3_010
  | FMUL_RUP_Q_32Q     => Some fct3_011
  | FMUL_RMM_Q_32Q     => Some fct3_100
  | FMUL_DYN_Q_32Q     => Some fct3_111
  | FDIV_RNE_Q_32Q     => Some fct3_000
  | FDIV_RTZ_Q_32Q     => Some fct3_001
  | FDIV_RDN_Q_32Q     => Some fct3_010
  | FDIV_RUP_Q_32Q     => Some fct3_011
  | FDIV_RMM_Q_32Q     => Some fct3_100
  | FDIV_DYN_Q_32Q     => Some fct3_111
  | FSQRT_RNE_Q_32Q    => Some fct3_000
  | FSQRT_RTZ_Q_32Q    => Some fct3_001
  | FSQRT_RDN_Q_32Q    => Some fct3_010
  | FSQRT_RUP_Q_32Q    => Some fct3_011
  | FSQRT_RMM_Q_32Q    => Some fct3_100
  | FSQRT_DYN_Q_32Q    => Some fct3_111
  | FSGNJ_Q_32Q        => Some fct3_000
  | FSGNJN_Q_32Q       => Some fct3_001
  | FSGNJX_Q_32Q       => Some fct3_010
  | FMIN_Q_32Q         => Some fct3_000
  | FMAX_Q_32Q         => Some fct3_001
  | FCVT_RNE_S_Q_32Q   => Some fct3_000
  | FCVT_RTZ_S_Q_32Q   => Some fct3_001
  | FCVT_RDN_S_Q_32Q   => Some fct3_010
  | FCVT_RUP_S_Q_32Q   => Some fct3_011
  | FCVT_RMM_S_Q_32Q   => Some fct3_100
  | FCVT_DYN_S_Q_32Q   => Some fct3_111
  | FCVT_RNE_Q_S_32Q   => Some fct3_000
  | FCVT_RTZ_Q_S_32Q   => Some fct3_001
  | FCVT_RDN_Q_S_32Q   => Some fct3_010
  | FCVT_RUP_Q_S_32Q   => Some fct3_011
  | FCVT_RMM_Q_S_32Q   => Some fct3_100
  | FCVT_DYN_Q_S_32Q   => Some fct3_111
  | FCVT_RNE_D_Q_32Q   => Some fct3_000
  | FCVT_RTZ_D_Q_32Q   => Some fct3_001
  | FCVT_RDN_D_Q_32Q   => Some fct3_010
  | FCVT_RUP_D_Q_32Q   => Some fct3_011
  | FCVT_RMM_D_Q_32Q   => Some fct3_100
  | FCVT_DYN_D_Q_32Q   => Some fct3_111
  | FCVT_RNE_Q_D_32Q   => Some fct3_000
  | FCVT_RTZ_Q_D_32Q   => Some fct3_001
  | FCVT_RDN_Q_D_32Q   => Some fct3_010
  | FCVT_RUP_Q_D_32Q   => Some fct3_011
  | FCVT_RMM_Q_D_32Q   => Some fct3_100
  | FCVT_DYN_Q_D_32Q   => Some fct3_111
  | FEQ_Q_32Q          => Some fct3_010
  | FLT_Q_32Q          => Some fct3_001
  | FLE_Q_32Q          => Some fct3_000
  | FCLASS_Q_32Q       => Some fct3_001
  | FCVT_RNE_W_Q_32Q   => Some fct3_000
  | FCVT_RTZ_W_Q_32Q   => Some fct3_001
  | FCVT_RDN_W_Q_32Q   => Some fct3_010
  | FCVT_RUP_W_Q_32Q   => Some fct3_011
  | FCVT_RMM_W_Q_32Q   => Some fct3_100
  | FCVT_DYN_W_Q_32Q   => Some fct3_111
  | FCVT_RNE_WU_Q_32Q  => Some fct3_000
  | FCVT_RTZ_WU_Q_32Q  => Some fct3_001
  | FCVT_RDN_WU_Q_32Q  => Some fct3_010
  | FCVT_RUP_WU_Q_32Q  => Some fct3_011
  | FCVT_RMM_WU_Q_32Q  => Some fct3_100
  | FCVT_DYN_WU_Q_32Q  => Some fct3_111
  | FCVT_RNE_Q_W_32Q   => Some fct3_000
  | FCVT_RTZ_Q_W_32Q   => Some fct3_001
  | FCVT_RDN_Q_W_32Q   => Some fct3_010
  | FCVT_RUP_Q_W_32Q   => Some fct3_011
  | FCVT_RMM_Q_W_32Q   => Some fct3_100
  | FCVT_DYN_Q_W_32Q   => Some fct3_111
  | FCVT_RNE_Q_WU_32Q  => Some fct3_000
  | FCVT_RTZ_Q_WU_32Q  => Some fct3_001
  | FCVT_RDN_Q_WU_32Q  => Some fct3_010
  | FCVT_RUP_Q_WU_32Q  => Some fct3_011
  | FCVT_RMM_Q_WU_32Q  => Some fct3_100
  | FCVT_DYN_Q_WU_32Q  => Some fct3_111
  | FLQ_64Q            => Some fct3_100
  | FSQ_64Q            => Some fct3_100
  | FMADD_RNE_Q_64Q    => Some fct3_000
  | FMADD_RTZ_Q_64Q    => Some fct3_001
  | FMADD_RDN_Q_64Q    => Some fct3_010
  | FMADD_RUP_Q_64Q    => Some fct3_011
  | FMADD_RMM_Q_64Q    => Some fct3_100
  | FMADD_DYN_Q_64Q    => Some fct3_111
  | FMSUB_RNE_Q_64Q    => Some fct3_000
  | FMSUB_RTZ_Q_64Q    => Some fct3_001
  | FMSUB_RDN_Q_64Q    => Some fct3_010
  | FMSUB_RUP_Q_64Q    => Some fct3_011
  | FMSUB_RMM_Q_64Q    => Some fct3_100
  | FMSUB_DYN_Q_64Q    => Some fct3_111
  | FNMSUB_RNE_Q_64Q   => Some fct3_000
  | FNMSUB_RTZ_Q_64Q   => Some fct3_001
  | FNMSUB_RDN_Q_64Q   => Some fct3_010
  | FNMSUB_RUP_Q_64Q   => Some fct3_011
  | FNMSUB_RMM_Q_64Q   => Some fct3_100
  | FNMSUB_DYN_Q_64Q   => Some fct3_111
  | FNMADD_RNE_Q_64Q   => Some fct3_000
  | FNMADD_RTZ_Q_64Q   => Some fct3_001
  | FNMADD_RDN_Q_64Q   => Some fct3_010
  | FNMADD_RUP_Q_64Q   => Some fct3_011
  | FNMADD_RMM_Q_64Q   => Some fct3_100
  | FNMADD_DYN_Q_64Q   => Some fct3_111
  | FADD_RNE_Q_64Q     => Some fct3_000
  | FADD_RTZ_Q_64Q     => Some fct3_001
  | FADD_RDN_Q_64Q     => Some fct3_010
  | FADD_RUP_Q_64Q     => Some fct3_011
  | FADD_RMM_Q_64Q     => Some fct3_100
  | FADD_DYN_Q_64Q     => Some fct3_111
  | FSUB_RNE_Q_64Q     => Some fct3_000
  | FSUB_RTZ_Q_64Q     => Some fct3_001
  | FSUB_RDN_Q_64Q     => Some fct3_010
  | FSUB_RUP_Q_64Q     => Some fct3_011
  | FSUB_RMM_Q_64Q     => Some fct3_100
  | FSUB_DYN_Q_64Q     => Some fct3_111
  | FMUL_RNE_Q_64Q     => Some fct3_000
  | FMUL_RTZ_Q_64Q     => Some fct3_001
  | FMUL_RDN_Q_64Q     => Some fct3_010
  | FMUL_RUP_Q_64Q     => Some fct3_011
  | FMUL_RMM_Q_64Q     => Some fct3_100
  | FMUL_DYN_Q_64Q     => Some fct3_111
  | FDIV_RNE_Q_64Q     => Some fct3_000
  | FDIV_RTZ_Q_64Q     => Some fct3_001
  | FDIV_RDN_Q_64Q     => Some fct3_010
  | FDIV_RUP_Q_64Q     => Some fct3_011
  | FDIV_RMM_Q_64Q     => Some fct3_100
  | FDIV_DYN_Q_64Q     => Some fct3_111
  | FSQRT_RNE_Q_64Q    => Some fct3_000
  | FSQRT_RTZ_Q_64Q    => Some fct3_001
  | FSQRT_RDN_Q_64Q    => Some fct3_010
  | FSQRT_RUP_Q_64Q    => Some fct3_011
  | FSQRT_RMM_Q_64Q    => Some fct3_100
  | FSQRT_DYN_Q_64Q    => Some fct3_111
  | FSGNJ_Q_64Q        => Some fct3_000
  | FSGNJN_Q_64Q       => Some fct3_001
  | FSGNJX_Q_64Q       => Some fct3_010
  | FMIN_Q_64Q         => Some fct3_000
  | FMAX_Q_64Q         => Some fct3_001
  | FCVT_RNE_S_Q_64Q   => Some fct3_000
  | FCVT_RTZ_S_Q_64Q   => Some fct3_001
  | FCVT_RDN_S_Q_64Q   => Some fct3_010
  | FCVT_RUP_S_Q_64Q   => Some fct3_011
  | FCVT_RMM_S_Q_64Q   => Some fct3_100
  | FCVT_DYN_S_Q_64Q   => Some fct3_111
  | FCVT_RNE_Q_S_64Q   => Some fct3_000
  | FCVT_RTZ_Q_S_64Q   => Some fct3_001
  | FCVT_RDN_Q_S_64Q   => Some fct3_010
  | FCVT_RUP_Q_S_64Q   => Some fct3_011
  | FCVT_RMM_Q_S_64Q   => Some fct3_100
  | FCVT_DYN_Q_S_64Q   => Some fct3_111
  | FCVT_RNE_D_Q_64Q   => Some fct3_000
  | FCVT_RTZ_D_Q_64Q   => Some fct3_001
  | FCVT_RDN_D_Q_64Q   => Some fct3_010
  | FCVT_RUP_D_Q_64Q   => Some fct3_011
  | FCVT_RMM_D_Q_64Q   => Some fct3_100
  | FCVT_DYN_D_Q_64Q   => Some fct3_111
  | FCVT_RNE_Q_D_64Q   => Some fct3_000
  | FCVT_RTZ_Q_D_64Q   => Some fct3_001
  | FCVT_RDN_Q_D_64Q   => Some fct3_010
  | FCVT_RUP_Q_D_64Q   => Some fct3_011
  | FCVT_RMM_Q_D_64Q   => Some fct3_100
  | FCVT_DYN_Q_D_64Q   => Some fct3_111
  | FEQ_Q_64Q          => Some fct3_010
  | FLT_Q_64Q          => Some fct3_001
  | FLE_Q_64Q          => Some fct3_000
  | FCLASS_Q_64Q       => Some fct3_001
  | FCVT_RNE_W_Q_64Q   => Some fct3_000
  | FCVT_RTZ_W_Q_64Q   => Some fct3_001
  | FCVT_RDN_W_Q_64Q   => Some fct3_010
  | FCVT_RUP_W_Q_64Q   => Some fct3_011
  | FCVT_RMM_W_Q_64Q   => Some fct3_100
  | FCVT_DYN_W_Q_64Q   => Some fct3_111
  | FCVT_RNE_WU_Q_64Q  => Some fct3_000
  | FCVT_RTZ_WU_Q_64Q  => Some fct3_001
  | FCVT_RDN_WU_Q_64Q  => Some fct3_010
  | FCVT_RUP_WU_Q_64Q  => Some fct3_011
  | FCVT_RMM_WU_Q_64Q  => Some fct3_100
  | FCVT_DYN_WU_Q_64Q  => Some fct3_111
  | FCVT_RNE_Q_W_64Q   => Some fct3_000
  | FCVT_RTZ_Q_W_64Q   => Some fct3_001
  | FCVT_RDN_Q_W_64Q   => Some fct3_010
  | FCVT_RUP_Q_W_64Q   => Some fct3_011
  | FCVT_RMM_Q_W_64Q   => Some fct3_100
  | FCVT_DYN_Q_W_64Q   => Some fct3_111
  | FCVT_RNE_Q_WU_64Q  => Some fct3_000
  | FCVT_RTZ_Q_WU_64Q  => Some fct3_001
  | FCVT_RDN_Q_WU_64Q  => Some fct3_010
  | FCVT_RUP_Q_WU_64Q  => Some fct3_011
  | FCVT_RMM_Q_WU_64Q  => Some fct3_100
  | FCVT_DYN_Q_WU_64Q  => Some fct3_111
  | FCVT_RNE_L_Q_64Q   => Some fct3_000
  | FCVT_RTZ_L_Q_64Q   => Some fct3_001
  | FCVT_RDN_L_Q_64Q   => Some fct3_010
  | FCVT_RUP_L_Q_64Q   => Some fct3_011
  | FCVT_RMM_L_Q_64Q   => Some fct3_100
  | FCVT_DYN_L_Q_64Q   => Some fct3_111
  | FCVT_RNE_LU_Q_64Q  => Some fct3_000
  | FCVT_RTZ_LU_Q_64Q  => Some fct3_001
  | FCVT_RDN_LU_Q_64Q  => Some fct3_010
  | FCVT_RUP_LU_Q_64Q  => Some fct3_011
  | FCVT_RMM_LU_Q_64Q  => Some fct3_100
  | FCVT_DYN_LU_Q_64Q  => Some fct3_111
  | FCVT_RNE_Q_L_64Q   => Some fct3_000
  | FCVT_RTZ_Q_L_64Q   => Some fct3_001
  | FCVT_RDN_Q_L_64Q   => Some fct3_010
  | FCVT_RUP_Q_L_64Q   => Some fct3_011
  | FCVT_RMM_Q_L_64Q   => Some fct3_100
  | FCVT_DYN_Q_L_64Q   => Some fct3_111
  | FCVT_RNE_Q_LU_64Q  => Some fct3_000
  | FCVT_RTZ_Q_LU_64Q  => Some fct3_001
  | FCVT_RDN_Q_LU_64Q  => Some fct3_010
  | FCVT_RUP_Q_LU_64Q  => Some fct3_011
  | FCVT_RMM_Q_LU_64Q  => Some fct3_100
  | FCVT_DYN_Q_LU_64Q  => Some fct3_111
  | _                  => None
  end.
