// Copyright (c) 2019, Massachusetts Institute of Technology
//
// Author: Clément Pit-Claudel <cpitcla@csail.mit.edu>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "../mmio.h"

// All images generated with
//   TerminalImageViewer/src/main/cpp/tiv -256 -w 80 -h 38 -0 koika.png

const char* const koika[] = {
  "[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;234m[38;5;252m▄[48;5;188m[38;5;232m▄[48;5;255m[38;5;16m▄[48;5;236m[38;5;188m▄[48;5;16m[38;5;59m▄[48;5;16m[38;5;236m▄[48;5;16m[38;5;16m▄[48;5;233m[38;5;234m▄[48;5;16m[38;5;188m▄[48;5;16m[38;5;240m▄[48;5;145m[38;5;187m▄[48;5;232m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[0m",
  "[48;5;181m[38;5;255m▄[48;5;188m[38;5;59m▄[48;5;233m[38;5;145m▄[48;5;16m[38;5;145m▄[48;5;16m[38;5;188m▄[48;5;255m[38;5;255m▄[48;5;239m[38;5;102m▄[48;5;232m[38;5;145m▄[48;5;233m[38;5;188m▄[48;5;188m[38;5;145m▄[48;5;145m[38;5;188m▄[48;5;255m[38;5;255m▄[48;5;248m[38;5;255m▄[48;5;248m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;16m[38;5;224m▄[48;5;16m[38;5;235m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[0m",
  "[48;5;238m[38;5;16m▄[48;5;237m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;233m▄[48;5;145m[38;5;255m▄[48;5;240m[38;5;188m▄[48;5;248m[38;5;102m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;244m[38;5;16m▄[48;5;255m[38;5;240m▄[48;5;255m[38;5;188m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;95m[38;5;255m▄[48;5;16m[38;5;180m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[0m",
  "[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;59m[38;5;16m▄[48;5;102m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;236m[38;5;16m▄[48;5;252m[38;5;232m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;187m▄[48;5;255m[38;5;246m▄[48;5;255m[38;5;248m▄[48;5;255m[38;5;224m▄[48;5;255m[38;5;216m▄[48;5;223m[38;5;252m▄[48;5;255m[38;5;209m▄[48;5;255m[38;5;180m▄[48;5;255m[38;5;255m▄[48;5;243m[38;5;255m▄[48;5;138m[38;5;255m▄[48;5;138m[38;5;255m▄[48;5;233m[38;5;255m▄[48;5;16m[38;5;181m▄[48;5;16m[38;5;232m▄[0m",
  "[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;238m[38;5;233m▄[48;5;102m[38;5;237m▄[48;5;255m[38;5;232m▄[48;5;224m[38;5;16m▄[48;5;235m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;233m[38;5;16m▄[48;5;144m[38;5;16m▄[48;5;244m[38;5;239m▄[48;5;96m[38;5;244m▄[48;5;95m[38;5;145m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;202m▄[48;5;255m[38;5;215m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;253m[38;5;255m▄[0m",
  "[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;250m[38;5;16m▄[48;5;216m[38;5;181m▄[48;5;216m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;222m[38;5;255m▄[48;5;215m[38;5;209m▄[48;5;216m[38;5;241m▄[0m",
  "[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;16m[38;5;16m▄[48;5;242m[38;5;16m▄[48;5;255m[38;5;180m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;255m[38;5;255m▄[48;5;216m[38;5;138m▄[48;5;243m[38;5;238m▄[0m",
  0
};

int main() {
  const char* const* lines = koika;
  while (*lines) {
    const char* str = *lines++;
    while (*str)
      putchar(*str++);
    putln();
  }
  putln();

  if (host_is_fpga()) {
    pause();
    putchars("\033c");
    main();
  }

  return 0;
}
